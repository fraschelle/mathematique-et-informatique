# Barêmes des présentations


Valider un critère rajoute un point dans la catégorie associée.


  - Présentation orale :
    - introduction et motivation
    - présentation fluide / bien préparée / compréhensible
    - réinsertion dans un cas d'usage
    - support adéquat (courbe et formule, extrait de code)
  - Maintenabilité : 
    - modularisation du code dans son ensemble / séparation _front_/_back_
    - atomisation / modularisation des fonctions
    - réutilisation possible du code (de sous-partie par exemple)
    - documentation présente
  - Qualité
    - tests réussis  > 70%
    - tests réussis à 100%
    - sujet maîtrisé
    - intérêt du sujet compris
  - Méthodologie et organisation du projet
    - usage de modules extérieurs pour la présentation ou la vérification 
    - usage de classe opportune / objets proposés adéquat
    - répartition des tâches efficace 
    - usage maîtrisé de Git : commits fréquents et bien documentés
  - Implication 
    - recherche extérieure / documentation réalisée
    - propositions d'améliorations
    - astuce mathématique / algorithmique innovante 
    - (tentative de) mise en production de module 
    
