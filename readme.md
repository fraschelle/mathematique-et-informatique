# Mathématique appliquée à l'informatique

Ces notes de cours survolent les notions de mathématiques appliquées à l'informatique. Les exemples de programmation sont donnés en Python.

Les notes sont disponibles publiquement 

 - sur le dépôt [framagit:fraschelle/mathematique-et-informatique](https://framagit.org/fraschelle/mathematique-et-informatique)
 - sur la page [fraschelle.frama.io/mathematique-et-informatique](https://fraschelle.frama.io/mathematique-et-informatique)


## Mots clés associés

 - arithmétique élémentaire, théorie des ensembles
 - fonctions et suites de variables réelles
 - algèbre linéaire, matrices et vecteurs
 - statistique et probabilité
 - dérivation et intégration, analyse
 - théorie des graphes, parcours de graphes
 - régression linéaire, interpolation, extrapolation


## Remarques / Suggestions

N'hésitez pas à faire vos remarques en soulevant des _issues_ sur le [dépôt framagit:fraschelle/mathematique-et-informatique](https://framagit.org/fraschelle/mathematique-et-informatique) associé à ce [JupyterBook](https://jupyterbook.org/).


## Construction du livre localement

Pour construire le livre localement, il faut tout d'abord cloner le dépôt 

```bash
git clone https://framagit.org/fraschelle/mathematique-et-informatique.git
```

avant de lancer

```bash
jupyter book build .
```

dans le répertoire du dépôt (`cd mathematique-et-informatique`).
