# Ressources générales

## (Ré)Visions de Mathématiques

Wiki-Université propose de nombreux cours de mathématique, pour la plupart fort bien illustrés.

 - [Portail de probabilité et statistique](https://fr.wikiversity.org/wiki/D%C3%A9partement:Statistique_et_probabilit%C3%A9s) 
 - [Matrices et calcul matriciel](https://fr.wikiversity.org/wiki/Matrice), pour y voir plus clair dans le formalisme de la régression linéaire
 - [Analyse numérique et calcul scientifique](https://fr.wikiversity.org/wiki/Analyse_num%C3%A9rique_et_calcul_scientifique), pour la regression et la représentation des données de façon générale, il convient d'avoir quelques notions de matrices et de leur manipulation

## Python

Le livre [Python programming](https://en.m.wikibooks.org/wiki/Python_Programming) de la collection des Wikibooks constitue une excellente introduction au langage Python. 

La [documentation officielle](https://www.python.org/doc/) et la documentation de ses librairies associées sont suffisantes pour manopuler le langage Python. Les [tutoriels de la documentation officielle](https://docs.python.org/3/tutorial/index.html) sont un très bon point de départ à l'apprentissage du langage.

### Libairies spécialisées

Les [tutoriels Numpy](https://numpy.org/numpy-tutorials/) sont un très bon point de départ pour l'utilisation avancée des matrices.

La [documentation de scipy](https://docs.scipy.org/doc/scipy/tutorial/index.html) contient de courtes notices d'introduction aux concepts développés dans la librairie.
