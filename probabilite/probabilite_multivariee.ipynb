{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4e81f585",
   "metadata": {},
   "source": [
    "# Loi de probabilité à plusieurs variables"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01b85d73",
   "metadata": {},
   "source": [
    "On appelle [statistique multivariée](https://fr.wikipedia.org/wiki/Statistique_multivari%C3%A9e) l'étude des réalisations des [phénomènes aléatoires dépendant de plusieurs variables](https://fr.wikipedia.org/wiki/Loi_de_probabilit%C3%A9_%C3%A0_plusieurs_variables). La statistique multivariée apparaît dans de nombreuses branches, puisqu'il est rare qu'un phénomène ne dépende que d'une seule variable aléatoire. Étudier les phénomènes multivariés, c'est donc étudier des phénomènes plus réalistes qu'à travers les loi de probabilités à une seule variable."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4d2a6e4",
   "metadata": {},
   "source": [
    "## Probabilité indépendante\n",
    "\n",
    "Deux évènements sont dit **indépendants** lorsque la réalisation de l'un n'affecte pas la réalisation de l'autre. Dans la pratique, il est difficile de justifier que deux évènements sont indépendants, on procède en général par calcul de [corrélations](https://fr.wikipedia.org/wiki/Corr%C3%A9lation_(statistiques)). Mathématiquement, l'indépendance permet certains calculs. Par exemple : \n",
    "\n",
    " - $\\mathbb{P}\\left(A\\cap B\\right) = \\mathbb{P}\\left(A\\right) \\times \\mathbb{P}\\left(B\\right)$\n",
    " - $\\mathbb{P}\\left(A\\cup B\\right) = \\mathbb{P}\\left(A\\right) + \\mathbb{P}\\left(B\\right) - \\mathbb{P}\\left(A\\right) \\times \\mathbb{P}\\left(B\\right)$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01608a8d",
   "metadata": {},
   "source": [
    "```{admonition} Probabilité indépendante et raisonnement\n",
    ":class: example\n",
    "On peut se servir de ces règles de calcul pour calculer certaines probabilités complexes. Ainsi, la probabilité d'obtenir un chiffre pair sur un dé à 6 faces se calcule comme la probabilité d'obtenir un $2$, un $4$ ou un $6$, chacune de ces probabilités étant supposée indépendant des autres.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb6979f0",
   "metadata": {},
   "source": [
    "## Probabilité conditionnelle\n",
    "\n",
    "Lorsque deux évènements ne sont pas indépendants, on peut calculer la [probabilité conditionnelle](https://fr.wikipedia.org/wiki/Probabilit%C3%A9_conditionnelle) que l'un se réalise si l'autre s'est (ou non) réalisé. On note la probabilité que l'évènement $A$ se produise sachant que l'évènement $B$ s'est produit sous la forme $\\mathbb{P}(A\\mid B)$. Connaissant la probabilité $\\mathbb{P}\\left(A \\cap B\\right)$ que les évènements $A$ et $B$ se produisent simultanément, on peut évaluer la probabilité conditionnelle de $A$ sachant $B$ (ou probabilité que l'évènement $A$ se produisent sachant que l'évènement $B$ a eu lieu) à l'aide de l'expression\n",
    "\n",
    "$$\n",
    "\\mathbb{P}\\left(A\\mid B\\right)=\\frac{\\mathbb{P}\\left(A \\cap B\\right)}{\\mathbb{P}\\left(B\\right)}\n",
    "$$\n",
    "\n",
    "Cette expression est sans doute plus facile à comprendre dans sa forme inversée : $\\mathbb{P}\\left(A \\cap B\\right) =  \\mathbb{P}\\left(A\\mid B\\right)\\mathbb{P}\\left(B\\right)$, où il est clair que les évènements $A$ et $B$ s'obtiennent simultanément lorsque l'évènement $B$ se produit (d'où le $\\mathbb{P}\\left(B\\right)$) et l'évènement $A$ se produit sachant que $B$ s'est produit.\n",
    "\n",
    "Lorsque les évènement $A$ et $B$ sont indépendant, on a $\\mathbb{P}\\left(A\\cap B\\right) = \\mathbb{P}\\left(A\\right) \\times \\mathbb{P}\\left(B\\right)$ et donc l'évènement $A$ schant $B$ est indépendant de l'évènement $B$, comme il se doit : $\\mathbb{P}\\left(A\\mid B\\right) = \\mathbb{P}\\left(A\\right)$.\n",
    "\n",
    "Les probabilités conditionnelles peuvent se représenter par des arbres de décision, comme ci-dessous.\n",
    "\n",
    "![Probabilité conditionnelle et arbre de décision](fig/Spieltheorie_Bayes.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cbad1da1",
   "metadata": {},
   "source": [
    "## Probabilité à priori, à postériori, et théorème de Bayes\n",
    "\n",
    "On parle de probabilité _à priori_ lorsque la probabilité est proposée / supposée à partir de connaissances empiriques. C'est par exemple le cas lorsque l'on énonce que la probabilité d'obtenir un $6$ sur un dé à six faces est _à priori_ de $1/6$. On parlera de probabilité _à postériori_ lorsque celle-ci est calculée une fois une expérience réalisée.\n",
    "\n",
    "Par exemple, on peut supposer _à priori_ que le dé avec lequel on joue n'est pas pipé, et donc que la probabilité d'obtenir un $6$ est de $1/6$. Puis on fait un milliers de lancers, et on se demande qu'elle est la probabililté _à postériori_ d'obtenir un $6$ pour ce dé. Ce genre de raisonnement s'appelle raisonnement bayésien, puisque la pierre angulaire du raisonnement est le [théorème de Bayes](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Bayes)\n",
    "\n",
    "$$\n",
    "\\mathbb{P}\\left(A \\mid B\\right) = \\dfrac{\\mathbb{P}\\left(B \\mid A\\right)\\mathbb{P}\\left(A\\right)}{\\mathbb{P}\\left(B\\right)}\n",
    "$$\n",
    "\n",
    "où $\\mathbb{P}\\left(B\\right) \\neq 0$, et\n",
    " \n",
    " - $\\mathbb{P}\\left(A \\mid B\\right)$ est la probabilité à postériori que l'évènement $A$ se réalise sachant que l'évènement $B$ s'est réalisé\n",
    " - $\\mathbb{P}\\left(B \\mid A\\right)$ est la probabilité à priori de $B$ sachant $A$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c139b54a",
   "metadata": {},
   "source": [
    "Le théorème de Bayes se démontre assez facilement pour deux évènements, puisque $\\mathbb{P}\\left(A \\mid B\\right)\\,\\mathbb{P}\\left(B\\right) = \\mathbb{P}\\left(A \\cap B\\right) = \\mathbb{P}\\left(B \\cap A\\right) = \\mathbb{P}\\left(B \\mid A\\right)\\,\\mathbb{P}\\left(B\\right)$ depuis le [théorème des probabilité totale](https://fr.wikipedia.org/wiki/Formule_des_probabilit%C3%A9s_totales). Voir également la figure ci-dessous.\n",
    "\n",
    "![Illustration du théorème de Bayes](fig/Bayes_Theorem_2D.svg.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04c87033",
   "metadata": {},
   "source": [
    "Lorsqu'on peut découper l'univers $\\Omega$ en une partition d'évènements $A_{i}$ deux-à-deux incompatibles (c'est-à-dire $A_{i}\\cap A_{j} = \\emptyset,\\, \\forall i\\neq j$ tandis que $\\bigcup_{i=1}^{n} A_{i} = \\Omega$), la probabilité $\\mathbb{P}\\left(B\\right) = \\sum_{i=1}^n \\mathbb{P}\\left(B | A_i\\right) \\mathbb{P}\\left(A_i\\right)$ s'exprime sous la forme d'une somme des probabilité à postériori, et on a donc \n",
    "\n",
    "$$\n",
    "\\mathbb{P}\\left(A \\mid B\\right) = \\dfrac{\\mathbb{P}\\left(B \\mid A\\right)\\mathbb{P}\\left(A\\right)}{\\sum_{i=1}^n \\mathbb{P}\\left(B | A_i\\right) \\mathbb{P}\\left(A_i\\right)}\n",
    "$$\n",
    "\n",
    "où toutes les probabilités sont expérimentales (à postériori). C'est sous cette forme qu'on utilise le plus souvent le théorème de Bayes pour les raisonnements bayésiens."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22d02525",
   "metadata": {},
   "source": [
    "## Probabilité jointe, probabilité marginale\n",
    "\n",
    "Jusqu'à présent, nous avons discuté des probabilité d'obtenir un évènement $A$ et/ou un évènement $B$, et de leur probabilités associées : indépendante, conditionnelle, à priori ou à postériori. Voyons maintenant le vocabulaire lorsque l'on ne parle plus d'évènements $A$ ou $B$, mais de variables aléatoires $X$ et $Y$, ces variables aléatoires pouvant se réaliser sous la forme de probabilité discrète ou continue, c'est-à-dire que $X$ (et/ou $Y$) prendra les valeurs $x$ prises dans un ensemble discret, ou continu. On note la loi de probabilité de ces deux variables aléatoires $\\mathbb{P}\\left(X,Y\\right)$ et on suppose à partir de maintenant que l'on ne peut plus séparer les variables (c'est-à-dire que les variables aléatoires $X$ et $Y$ sont dépendantes).\n",
    "\n",
    "On en parlera que de loi de probabilité à deux variables, pour ne pas compliquer à outrance le problème. Il est clair qu'augmenter le nombre de variables aléatoires complique essentiellement la calculabilité et la représentativité du problème, mais que les ordinateurs constituent justement le bon moyen de manipuler ce genre de données massives.\n",
    "\n",
    "Lorsque les probabilités des évènements $X$ et $Y$ ne sont pas indépendantes, on peut encore mesurer (ou donner à priori) la loi de probabilité jointe, c'est-à-dire la loi donnant la probabilité d'obtenir la valeur $x$ pour la variable aléatoire $X$ et la valeur $y$ pour la variable aléatoire $Y$. Dans le cas de variables aléatoire discrète, on met alors toutes ces valeurs dans un tableau, cf. [cet exemple](https://fr.wikipedia.org/wiki/Loi_de_probabilit%C3%A9_marginale#Loi_discr%C3%A8te). Dans le cas des variables continues, la représentation est beaucoup plus délicate. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77e67603",
   "metadata": {},
   "source": [
    "### Loi de probabilité marginale\n",
    "\n",
    "Quoi qu'il en soit, lorsque l'on somme l'ensemble des probabilités de l'ensemble des réalisations possibles de la variable aléatoire $Y$, on obtient la [loi marginale de $X$](https://fr.wikipedia.org/wiki/Loi_de_probabilit%C3%A9_marginale), noté $\\mathbb{P}_{Y}\\left(X\\right)$ : \n",
    "\n",
    "$$\n",
    "\\mathbb{P}_{Y}\\left(X\\right) = \\sum_{y} \\mathbb{P}\\left(X,Y=y\\right)\n",
    "$$\n",
    "\n",
    "et de même la loi marginale de $Y$ s'écrit alors $\\mathbb{P}_{X}\\left(Y\\right) = \\sum_{x} \\mathbb{P}\\left(X=x,Y\\right)$\n",
    "\n",
    "Cette probabilité est souvent rajoutée dans une nouvelle colonne (ou ligne pour $\\mathbb{P}_{X}\\left(Y\\right)$) du tableau, c'est-à-dire _à la marge_ du tableau de $\\mathbb{P}\\left(X,Y\\right)$, d'où son nom de loi _marginale_."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60467967",
   "metadata": {},
   "source": [
    "### Loi de probabilité construite sur la loi de probabilité conjointe\n",
    "\n",
    "Une fois donnée la loi de probabilité jointe $\\mathbb{P}\\left(X,Y\\right)$, on peut construire des lois de probabilité plus élaborée, comme par exemple la loi de probabilité somme de $X$ et de $Y$ : $Z=X+Y$ qui s'exprimera à travers la probabilité d'obtenir la valeur $Z=z=x+y$ lorsque la variable aléatoire $X$ se réalise à $x$ et la variable aléatoire $Y$ se réalise à $y$. On écrit encore \n",
    "\n",
    "$$\n",
    "\\mathbb{P}\\left(Z=z\\right) = \\sum_{x,y}\\mathbb{P}\\left(X=x, Y=y \\,|\\, x+y=z\\right) = \\sum_{x,y}\\mathbb{P}\\left(X=x, Y=y\\right)\\delta_{x+y,z}\n",
    "$$\n",
    "\n",
    "avec $\\delta_{i,j}$ le symbole de Kronecker $\\delta_{i,j}=1$ ssi $i=j$ et sinon $\\delta_{i,j}=0$."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
