#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Probability class represent several laws of probability.
"""

from typing import List, Sequence, Tuple
from math import atan2, cos, erf, exp, factorial, log, pi, sin, sqrt
from random import randint, random


def mean(sequence: Sequence[float]) -> float:
    """The estimator of the mathematical expected value, applied to a statistical sample.

    Parameters
    ----------
    sequence : Sequence[float]
        Sequence of floats.

    Returns
    -------
    mean : float
        The estimator of the mean.
    """
    res, n = 0.0, 0
    for value in sequence:
        res = (n*res + value) / (n+1)
        n += 1
    return res


def variance(sequence: Sequence[float]) -> float:
    """The estimator of the mathematical variance, applied to a statistical sample.

    Parameters
    ----------
    sequence : Sequence[float]
        Sequence of floats.

    Returns
    -------
    variance : float
        The estimator of the variance.
    """
    temp, mean, n = 0.0, 0.0, 0
    for value in sequence:
        temp = (n*temp + value**2) / (n+1)
        mean = (n*mean + value) / (n+1)
        n += 1
    return temp - mean**2


def estimators(sequence: Sequence[float]) -> Tuple[float, float]:
    """The estimators of the mathematical expected value and variance, applied to a statistical sample in the form of a sequence.

    Parameters
    ----------
    sequence : Sequence[float]
        Sequence of floats.

    Returns
    -------
    Tuple[float, float]
        The estimator of the expected value and of the variance, respectively.
    """
    temp, mean, n = 0.0, 0.0, 0
    for value in sequence:
        temp = (n*temp + value**2) / (n+1)
        mean = (n*mean + value) / (n+1)
        n += 1
    return mean, temp - mean**2


def C(n: int, k: int) -> int:
    """Combination coefficient Cnk, or k-combinations taken among n possible distinct states, also known as binomial coefficient.
    
    We use an efficient algorithm to compute the binomial coefficient.
    See https://en.wikipedia.org/wiki/Binomial_coefficient#Multiplicative_formula for more details. 
    

    Parameters
    ----------
    n : int
        total number of possible states.
    k : int
        size of the combinaison.

    Raises
    ------
    ValueError
        when n is smaller than k.

    Returns
    -------
    int
        The binomial coefficient, or k-combinations taken among n possible distinct states.

    """
    if n < 2*k: 
        if n < k:
            raise ValueError("n must be larger than k")
        return C(n, n-k)
    res = 1
    for i in range(1, k+1):
        res = (res*(n-i+1)) // i
    return res


class Binomial():
    def __init__(self, n: int, p: float):
        if n < 0:
            raise ValueError("n must be positive int")
        if not 0 <= p <= 1:
            raise ValueError("p must be a probability in [0,1] range")
        self.n = int(n)
        self.p = float(p)
        return None
    def __call__(self, k: int) -> float:
        k = int(k)
        return C(self.n, k) * self.p**k * (1-self.p)**(self.n-k)
    def cdf(self, k: int) -> float:
        return sum(self(x) for x in range(k+1))
    def fit(self, sequence: Sequence[float]) -> Tuple[float, float]:
        m, v = estimators(sequence)
        self.p = 1 - v/m
        self.n = m / self.p
        return self.n, self.p
    def sample(self, size: int=100) -> List[int]:
        y = [1 if (random() - (1-self.p)) > 0 else 0 for _ in range(size)]
        return y
        
class Poisson():
    def __init__(self, rate: float):
        if rate < 0:
            raise ValueError("lambda must be positive float")
        self.rate = float(rate)
        return None
    def __call__(self, k: int) -> float:
        k = int(k)
        return self.rate**k * exp(-self.rate) / factorial(k)
    def cdf(self, k: int) -> float:
        return sum(self(x) for x in range(k+1))
    def fit(self, sequence: Sequence[float]) -> float:
        m, v = estimators(sequence)
        self.rate = mean((m,v))
        return self.rate
    def sample(self, size: int=100) -> List[int]:
        return NotImplementedError

class Normal():
    """Gaussian law, or normal law. 
    By default, the centered reduced normal is returned.
    See https://en.wikipedia.org/wiki/Normal_distribution"""
    def __init__(self, mu: float=0.0, sigma: float=1.0):
        if sigma < 0:
            raise ValueError("sigma must be positive")
        self.mu = float(mu)
        self.sigma = float(sigma)
        return None
    def __call__(self, x: float) -> float:
        numer = exp(-(x-self.mu)*(x-self.mu)/2/self.sigma/self.sigma)
        denom = sqrt(2*pi)*self.sigma
        return numer/denom
    def cdf(self, x: float):
        return (1+erf((x-self.mu)/self.sigma/sqrt(2)))/2
    def fit(self, sequence: Sequence[float]):
        m, v = estimators(sequence)
        self.mu = m
        self.sigma = sqrt(v)
        return self.mu, self.sigma
    def sample(self, size: int=100) -> List[float]:
        """By the Box-Muller method. 
        See https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform"""
        if size%2:
            raise ValueError("requires an even number of points in the sample")
        samp = [self.mu for _ in range(size)]
        for i in range(size//2):
            x = sqrt(-2*log(random()))
            u = 2*pi*random()
            samp[2*i] = self.sigma*x*cos(u) + self.mu
            samp[2*i+1] = self.sigma*x*sin(u) + self.mu
        return samp

class Uniform():
    def __init__(self, a: float=0.0, b: float=1.0):
        if a > b:
            raise ValueError("a must be larger than b")
        self.a = float(a)
        self.b = float(b)
        return None
    def __call__(self, x: float) -> float:
        if x < self.a or x > self.b:
            return 0.0
        return 1/(self.b-self.a)
    def cdf(self, x: float) -> float:
        if x < self.a:
            return 0.0
        if self.b < x:
            return 1.0
        return (x-self.a)/(self.b-self.a)
    def fit(self, sequence: Sequence[float]) -> Tuple[float, float]:
        m, v = estimators(sequence)
        self.a, self.b = m+sqrt(12*v), m-sqrt(12*v)
        return self.a, self.b
    def sample(self, size: int=100) -> List[float]:
        return [self.a + random()*(self.b-self.a) for _ in range(size)]

class Cauchy():
    """Cauchy law.
    See https://en.wikipedia.org/wiki/Cauchy_distribution"""
    def __init__(self, a: float=0.0, x0: float=0.0):
        if a < 0:
            raise ValueError("a must be positive")
        self.a = float(a)
        self.x0 = float(x0)
        return None
    def __call__(self, x: float) -> float:
        return self.a/((x-self.x0)*(x-self.x0) + self.a*self.a)/pi
    def cdf(self, x: float) -> float:
        return atan2(x-self.x0, self.a)/pi + 1/2
    def fit(self, sequence: Sequence[float]) -> NotImplementedError:
        """Does not exist."""
        return NotImplementedError
    def sample(self, size: int=100) -> List[float]:
        return NotImplementedError

class LogNormal():
    """LogNormal law.
    See https://en.wikipedia.org/wiki/Log-normal_distribution"""
    def __init__(self, mu: float=0.0, sigma: float=1.0):
        if sigma < 0:
            raise ValueError("sigma must be positive")
        self.mu = float(mu)
        self.sigma = float(sigma)
        return None
    def __call__(self, x: float) -> float:
        if x < 0.0:
            raise ValueError("x must be positive")
        numer = exp(-(log(x)-self.mu)*(log(x)-self.mu)/2/self.sigma/self.sigma)
        denom = x*self.sigma*sqrt(2*pi)
        return numer/denom
    def cdf(self, x: float) -> float:
        if x < 0.0:
            raise ValueError("x must be positive")
        return (1+erf((log(x)-self.mu)/self.sigma/sqrt(2)))/2
    def fit(self, sequence: Sequence[float]):
        m, v = estimators(sequence)
        self.mu = log(m) - v/2
        self.sigma = log(1+v/m/m)
        return self.mu, self.sigma
    def sample(self, size: int=100) -> List[float]:
        return NotImplementedError
