{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimisation linéaire\n",
    "\n",
    "Un [problème d'optimisation](https://fr.wikipedia.org/wiki/Optimisation_lin%C3%A9aire) consiste à minimiser/maximiser une fonction (dite de coût ou de gain) par rapport à un ensemble de paramètres bornés. Dans le cadre de l'optimisation linéaire, la fonction de coût est linéaire.\n",
    "\n",
    "Les problèmes d'optimisation sont à la base de la [programmation linéaire](https://fr.wikipedia.org/wiki/Optimisation_lin%C3%A9aire), les problèmes simples se calculent à l'aide de l'[algorithme du simplexe](https://fr.wikipedia.org/wiki/Algorithme_du_simplexe) et les problèmes d'optimisation linéaires répondent au [principe de dualité](https://fr.wikipedia.org/wiki/Dualit%C3%A9_(optimisation))."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimisation sans contrainte et sous contrainte\n",
    "\n",
    "Un problème d'optimisation sans contrainte est en général assez facile à résoudre. Il s'agit de trouver le minimum d'une fonction de coût, ou le maximum d'une fonction de gain. Il suffit alors de trouver les zéros de la dérivée de cette fonction de coût ou de gain. Cela se fait par [algorithme de descente de gradient](https://fr.wikipedia.org/wiki/Algorithme_du_gradient) par exemple.\n",
    "\n",
    "Un problème d'optimisation avec contrainte nécessite en revanche un peu plus d'efforts pour être résolu. On écrit ce genre de problème sous la forme\n",
    "\n",
    "$$\n",
    "\\left\\{\\begin{array}{l}\n",
    "{\\sup}_x\\;c^\\top x\\\\\n",
    "Ax\\leq b,\n",
    "\\end{array}\\right.\n",
    "$$\n",
    "\n",
    "où l'on cherche à obtenir le vecteur $x$ minimisant la fonction de coût $c^\\top x$ répondant à certains critères extérieurs représentés par la matrice $A$ et le vecteur $c$ (de sorte que $c^\\top x$ soit une sorte de produit vectoriel). \n",
    "\n",
    "Quelques remarques tout d'abord\n",
    "\n",
    " - toute la dépendance en $x$ de la fonction de coût $c^\\top x$ que l'on cherche à minimiser est explicite : le problème est linéaire\n",
    " - trouver le minimum ${\\inf}_{x}\\; c^\\top x$ d'une fonction de gain $c^\\top x$ revient à calculer le maximum ${\\sup}_{x} \\left(- c^\\top x\\right)$ de son opposé\n",
    " - toute la dépendance en $x$ des contraintes $Ax\\leq b$ est explicite : le problème est linéaire\n",
    " - si l'on veut renverser les signes d'inégalités : $Ax\\leq b \\Leftrightarrow Ax\\geq -b$\n",
    " \n",
    "Le problème linéaire ainsi posé est donc général : on peut calculer le maximum ou le minimum d'une fonction d'objectif soumis à des critères linéaires généraux. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Existence de solution\n",
    "\n",
    "On peut montrer que l'ensemble des solutions admissibles forme un polyèdre convexe (on peut tracer une ligne entre deux quelconques de ses points, cette ligne ne sortira jamais du polyèdre). De plus, s'il existe une solution, alors cette solution est nécessairement sur un sommet du polyèdre. \n",
    "\n",
    "Ce [théorême](https://fr.wikipedia.org/wiki/Optimisation_lin%C3%A9aire#Existence_de_solution) a une portée très forte, car il suffit de longer les côtés de ce polyèdre et de suivre la fonction de gain dans ce déplacement : la fonction de gain doit toujours croitre jusqu'à son maximum (que l'on cherche). C'est le principe de l'[algorithme du simplexe](https://fr.wikipedia.org/wiki/Algorithme_du_simplexe)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Réécriture du problème : les variables libres et la forme standard\n",
    "\n",
    "Un premier pas vers la solution consiste à réaliser que les inégalités $Ax\\geq b$ peuvent se mettre sous la forme d'égalité $Ax + s = b$, $s \\geq 0$ en modifiant la taille du vecteur $x$ et de la matrice $A$ (qui devient éventuellement rectangulaire). Les variables $s$ ainsi introduite sont appelées des variables _slack_ ou libres.\n",
    "\n",
    "Le problème de la recherche de l'ensemble des points $x$ tels que $Ax\\geq b$ : $\\left\\{x\\in\\mathbb{R}^n:Ax \\geqslant b\\right\\}$ est dit être en **forme canonique** tandis que la recherche sous la forme $\\left\\{x\\in\\mathbb{R}^n:Ax=b,~x\\geqslant 0\\right\\}$ est dit être en **forme standard**. C'est sous sa forme standard que l'algorithme du simplex peut être utilisé.\n",
    "\n",
    "On étudiera l'[exemple à deux dimensions](https://fr.wikipedia.org/wiki/Optimisation_lin%C3%A9aire#Introduction_sur_un_exemple_en_dimension_2) proposé par Wikipédia pour y voir plus clair."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Algorithme du simplex\n",
    "\n",
    "Une fois mis sous la forme standard, on résout le problème à l'aide de l'[algorithme du simplex](https://fr.wikipedia.org/wiki/Algorithme_du_simplexe) aussi appelé algorithme de Dantzig.\n",
    "\n",
    " 1. Trouver la position de la colonne pivot $s$ qui correspond au maximum des $c$\n",
    " 2. Si $c_{s} \\leq 0$ STOP : la solution optimale est trouvée\n",
    " 3. Si $A_{is} \\leq 0 \\;\\forall i$ STOP : la fonction de gain n'est pas bornée\n",
    " 3. Trouver la position de la ligne pivot $r$ qui correspond au minimum des $b_{i}/A_{is}$\n",
    " 3. Mettre à jour les tableaux $A$, $b$ et $c$.\n",
    " \n",
    "La mise à jour procède comme suit : \n",
    "\n",
    " - $b_{i} \\leftarrow b_{i} - \\frac{A_{is}b_{r}}{A_{rs}}\\;\\forall i\\neq r$\n",
    " - $b_{r} \\leftarrow \\frac{b_{r}}{A_{rs}}$ : la ligne pivot est divisée par l'élément pivot\n",
    " - $c_{j} \\leftarrow c_{j} - \\frac{c_{s}A_{rj}}{A_{rs}} \\; \\forall j$\n",
    " - $A_{ij} \\leftarrow A_{ij} - \\frac{A_{is}A_{rj}}{A_{rs}} \\; \\forall j\\neq s\\;\\forall i\\neq r$\n",
    " - $A_{is} \\leftarrow 0 \\; \\forall i\\neq r$ : la colonne pivot se met à zéro, sauf la case pivot\n",
    " - $A_{rj} \\leftarrow \\frac{A_{rj}}{A_{rs}} \\; \\forall j$ : la ligne pivot est divisée par l'élément pivot\n",
    "\n",
    "Lorsque l'algorithme est stopé, la solution optimale $x_{0}$ est lue dans la table des $b$; Attention toutefois, il faut avoir préalablement sauvegardé tous les pivots, et ne conserver que le _dernier_ pivot d'une ligne, qui va donner en regard de la colonne la coordonnées de $b$ de la solution. Les gains totaux sont obtenus grâce à la fonction de gain : $c^\\top x_{0}$."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
