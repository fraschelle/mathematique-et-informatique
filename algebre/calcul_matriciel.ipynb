{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calcul matriciel\n",
    "\n",
    "Si la [multiplication matricielle](https://fr.wikipedia.org/wiki/Produit_matriciel) est sans doute l'élément d'algèbre matricielle le plus important puisqu'il représente la composition d'applications linéaires, les autres opérations que sont l'[inversion de matrice](https://fr.wikipedia.org/wiki/Matrice_inversible) et la [diagonalisation de matrice](https://fr.wikipedia.org/wiki/Matrice_diagonale) sont également d'importance puisqu'elles représentent alors l'application inverse et la meilleure base dans laquelle l'application aura une représentation optimale.\n",
    "\n",
    "On ne s'intéresse dans la suite qu'au cas des matrices carrées, de taille $n\\times n$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Avant de pouvoir discuter de matrice diagonale ou inverse, on doit définir le [**déterminant** d'une matrice](https://fr.wikipedia.org/wiki/D%C3%A9terminant_(math%C3%A9matiques)) et la [**commatrice**](https://fr.wikipedia.org/wiki/Comatrice). Attention, ces notions sont très proches, mais ne doivent pas être confondues."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Déterminant d'une matrice carrée\n",
    "\n",
    "Le déterminant d'une matrice carrée se calcule par récurence. \n",
    "\n",
    "Le déterminant $\\det A$ d'une matrice $2\\times 2$ se calcule comme \n",
    "\n",
    "$$\n",
    "A = \\begin{pmatrix}\n",
    "a_{11} & a_{12} \\\\\n",
    "a_{21} & a_{22}\n",
    "\\end{pmatrix}\n",
    "\\Rightarrow \\det A = \n",
    "\\begin{vmatrix} a_{11} & a_{12} \\\\\n",
    "a_{21} & a_{22} \\end{vmatrix} \n",
    "= a_{11}a_{22} - a_{12}a_{21}\n",
    "$$\n",
    "\n",
    "Le déterminant d'une matrice $A_{n\\times n}$ de taille $n\\times n$ se calcule alors par réduction dimensionnelle\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "\\det A &= \n",
    "\\begin{vmatrix} \n",
    "a_{11} & a_{12} & \\cdots & a_{1n} \\\\\n",
    "a_{21} & a_{22} & \\cdots & a_{2n} \\\\\n",
    "\\vdots & \\vdots & \\ddots & \\vdots \\\\\n",
    "a_{n1} & a_{n2} & \\cdots & a_{nn}\n",
    " \\end{vmatrix} \\\\\n",
    " & = \n",
    " a_{11} \\begin{vmatrix} \n",
    "a_{12} & \\cdots & a_{1n} \\\\\n",
    "a_{22} & \\cdots & a_{2n} \\\\\n",
    "\\vdots & \\ddots & \\vdots \\\\\n",
    "a_{n2} & \\cdots & a_{nn}\n",
    " \\end{vmatrix}\n",
    " - a_{21} \\begin{vmatrix} \n",
    "a_{12} & \\cdots & a_{1n} \\\\\n",
    "a_{22} & \\cdots & a_{2n} \\\\\n",
    "\\vdots & \\ddots & \\vdots \\\\\n",
    "a_{n2} & \\cdots & a_{nn}\n",
    " \\end{vmatrix}\n",
    " + \\cdots + \\left(-1\\right)^{n+1} a_{n1} \n",
    " \\begin{vmatrix} \n",
    "a_{12} & \\cdots & a_{1n} \\\\\n",
    "a_{22} & \\cdots & a_{2n} \\\\\n",
    "\\vdots & \\ddots & \\vdots \\\\\n",
    "a_{n2} & \\cdots & a_{nn}\n",
    " \\end{vmatrix}\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "où l'on a développé selon la première colonne (on peut développer selon n'importe quelle colonne ou ligne, en gardant la signature), cela consiste à progresser par ligne, le long de la première colonne. Par exemple pour la ligne $i$, la méthode consiste à \n",
    "\n",
    " - extraire le premier coefficient $a_{i1}$ de la ligne $i$, et le multiplier par la signature $\\left(-1\\right)^{i+1}$\n",
    " - construire la sous-matrice $\\bar{A}_{i1}$ qui ne contient plus ni la première colonne ni la ligne $i$\n",
    " - multiplier le coefficient $\\left(-1\\right)^{i+1}a_{i1}$ par le déterminant de la sous-matrice $\\bar{A}_{i1}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Déterminant d'une matrice $3\\times 3$\n",
    ":class: example\n",
    "Soit \n",
    "\n",
    "$$\n",
    "A = \\begin{pmatrix}\n",
    "a_{11} & a_{12} & a_{13} \\\\\n",
    "a_{21} & a_{22} & a_{23} \\\\\n",
    "a_{31} & a_{32} & a_{33}\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "alors son déterminant sera \n",
    "\n",
    "$$\n",
    "\\det A = a_{11}\n",
    "\\begin{vmatrix}\n",
    "a_{22} & a_{23} \\\\\n",
    "a_{32} & a_{33}\n",
    "\\end{vmatrix}\n",
    "- a_{21} \n",
    "\\begin{vmatrix}\n",
    "a_{12} & a_{13} \\\\\n",
    "a_{32} & a_{33}\n",
    "\\end{vmatrix}\n",
    "+ a_{31} \n",
    "\\begin{vmatrix}\n",
    "a_{12} & a_{13} \\\\\n",
    "a_{22} & a_{23}\n",
    "\\end{vmatrix}\n",
    "$$\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On aurait pu développer le déterminant par n'importe quelle ligne ou colonne, mais il faut faire attention à la signature : on multiplie par $-1$ chaque fois que l'on se déplace d'une ligne ou d'une colonne. On a tout intérêt à prendre une ligne ou une colonne contenant le maximum de $0$ pour ne pas avoir trop de calculs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Comatrice\n",
    "\n",
    "La [comatrice](https://fr.wikipedia.org/wiki/Comatrice) $\\operatorname{com} A$ de la matrice $A$ detaille $n\\times n$ se calcule à l'aide de son déterminant. C'est une matrice de même taille que $A$ dont l'élément $\\left(i,j\\right)$ s'écrit\n",
    "\n",
    "$$\n",
    "\\left(\\operatorname{com}A\\right)_{i,j}=\\left(-1\\right)^{i+j}\\det\\left(A_{i,j}\\right)\n",
    "$$\n",
    "\n",
    "où $A_{i,j}$ est la sous-matrice carrée de taille $n-1$ déduite de $A$ en supprimant la $i$-ème ligne et la $j$-ème colonne. \n",
    "\n",
    "Par définition, la comatrice d'un nombre est ce nombre. On aura donc pour une matrice $2\\times 2$\n",
    "\n",
    "$$\n",
    "\\operatorname{com}\\begin{pmatrix} a & b \\\\ c & d \\end{pmatrix}=\\begin{pmatrix} d & -c\\\\ -b & a \\end{pmatrix}\n",
    "$$\n",
    "\n",
    "Pour une matrice $3\\times 3$, on aura la formule explicite\n",
    "\n",
    "$$\n",
    "\\operatorname{com}\\begin{pmatrix} a_{11} & a_{12} & a_{13}\\\\\n",
    "                    a_{21} & a_{22} & a_{23}\\\\\n",
    "                    a_{31} & a_{32} & a_{33}\\\\\n",
    "                    \\end{pmatrix}=\\begin{pmatrix} \n",
    "+\\begin{vmatrix} a_{22} & a_{23} \\\\  a_{32} & a_{33}\\end{vmatrix} & -\\begin{vmatrix} a_{21} & a_{23} \\\\  a_{31} & a_{33}\\end{vmatrix} & +\\begin{vmatrix} a_{21} & a_{22} \\\\  a_{31} & a_{32}\\end{vmatrix}\\\\\n",
    "-\\begin{vmatrix} a_{12} & a_{13} \\\\  a_{32} & a_{33}\\end{vmatrix} & +\\begin{vmatrix} a_{11} & a_{13} \\\\  a_{31} & a_{33}\\end{vmatrix} & -\\begin{vmatrix} a_{11} & a_{12} \\\\  a_{31} & a_{32}\\end{vmatrix}\\\\\n",
    "+\\begin{vmatrix} a_{12} & a_{13} \\\\  a_{22} & a_{23}\\end{vmatrix} & -\\begin{vmatrix} a_{11} & a_{13} \\\\  a_{21} & a_{23}\\end{vmatrix} & +\\begin{vmatrix} a_{11} & a_{12} \\\\  a_{21} & a_{22}\\end{vmatrix}\n",
    "\\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Déterminant d'une matrice $3\\times 3$\n",
    ":class: warning\n",
    "\n",
    "Les notions de déterminant et de comatrice sont donc très proches, gardons donc en tête que \n",
    " \n",
    " - la comatrice est une matrice (de même taille que la matrice d'origine)\n",
    " - le déterminant est un nombre\n",
    "\n",
    "Il existe une formule, dite **formule de Laplace**, qui relie comatrice et déterminant : \n",
    "\n",
    "$$\n",
    "\\det A=\\sum_{i=1}^na_{ij}(\\operatorname{com}A)_{i,j} =\\sum_{j=1}^na_{ij}(\\operatorname{com}A)_{i,j}\n",
    "$$\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inversion de matrice\n",
    "\n",
    "Inverser une matrice, c'est trouver la matrice dite **inverse** $A^{-1}$ de la matrice $A$ qui vaut \n",
    "\n",
    "$$\n",
    "A\\cdot A^{-1} = A^{-1}\\cdot A = \\mathbb{I_{n\\times n}}\n",
    "$$\n",
    "\n",
    "où $I_{n\\times n}$ est la matrice identité de taille $n$ : ses seuls éléments non-nuls sont sur la diagonale, et ils valent tous $1$.\n",
    "\n",
    "$$\n",
    "\\mathbb{I_{n\\times n}} = \n",
    "\\begin{pmatrix}\n",
    "1    & 0      & \\ldots & 0      \\\\\n",
    "0      & 1    & \\ddots & \\vdots \\\\\n",
    "\\vdots & \\ddots & \\ddots & 0      \\\\\n",
    "0      & \\ldots & 0      & 1\n",
    "\\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La matrice inverse $A^{-1}$ se calcule à l'aide de la formule\n",
    "\n",
    "$$\n",
    "A^{-1}=\\frac1{\\det A} \\,^{\\operatorname t}\\!{{\\rm com} A}\n",
    "$$\n",
    "\n",
    "C'est la transposée de la comatrice de $A$, divisée par le déterminant de $A$. \n",
    "\n",
    "Si on oublie le déterminant, on obtient une matrice diagonale dont les éléments sont $\\det A$, puisqu'on a la formule $A\\;{}^{\\operatorname t}\\!\\left(\\operatorname{com}A\\right)={}^{\\operatorname t}\\!\\left(\\operatorname{com}A\\right)\\;A=\\det(A)\\;\\mathbb{I_{n\\times n}}$. Néanmoins, il est clair que si $\\det A = 0$, la matrice $A$ n'est pas inversible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Diagonalisation de matrice\n",
    "\n",
    "Diagonaliser une matrice $A$, c'est trouver la matrice $P$, dite **matrice de passage**, qui vérifie $P^{-1}\\cdot A \\cdot P = D$, où $D$ est une matrice diagonale, dont les éléments non-nuls sont appelés les **valeurs propres** de la matrice $A$. Les colonnes de $P$ sont appelées les **vecteurs propres** de $A$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Polynôme caractéristique\n",
    "\n",
    "Soit $X$ un vecteur de taille $n$ et $A$ une matrice carrée de taille $n\\times n$. Soit alors à résoudre l'équation $A\\cdot X = \\lambda X$, où $\\lambda$ est un nombre. Il existe une infinité de solutions à cette équation, puisqu'on cherche les $n$ composantes de $X$ et le nombre $\\lambda$ avec seulement $n$ équations à notre disposition. \n",
    "\n",
    "Imaginons que l'on recommence la procédure $n$ fois, et que l'on obtienne $n$ vecteurs $X_{i=1\\cdots n}$ et $n$ valeurs $\\lambda_{i=1\\cdots n}$. On peut alors ranger ces vecteurs dans une matrice, qui est précisément la matrice $P$ de passage vers la base diagonale. \n",
    "\n",
    "Comment alors calculer ces vecteurs. Pour que le système sur-déterminé $A\\cdot X = \\lambda X$ ait un sens, il faut que $\\det \\left(A-\\lambda \\mathbb{I}\\right) = 0$. Cette équation est une simple équation polynomiale, et les valeurs propres $\\lambda_{i\\cdots n}$ sont les racines de cette équation. \n",
    "\n",
    "En effet, on aura\n",
    "\n",
    "$$\n",
    "\\det \\left(A-\\lambda \\mathbb{I}\\right) = \n",
    "\\begin{vmatrix} \n",
    "a_{11}-\\lambda & a_{12} & \\cdots & a_{1n} \\\\\n",
    "a_{21} & a_{22}-\\lambda & \\cdots & a_{2n} \\\\\n",
    "\\vdots & \\vdots & \\ddots & \\vdots \\\\\n",
    "a_{n1} & a_{n2} & \\cdots & a_{nn}-\\lambda\n",
    " \\end{vmatrix}\n",
    "$$\n",
    "\n",
    "et mettre ce déterminant à zéro consiste à trouver toutes les racines du **polynôme caractéristique** de la matrice $A$.\n",
    "\n",
    "### Matrice de passage des vecteurs propres\n",
    "\n",
    "Une fois toutes les valeurs propres obtenues, il suffit de résoudre l'ensemble des équations $A\\cdot X_{i} = \\lambda_{i} X_{i}$ pour chacune des values propres, et d'obtenir ainsi l'ensemble des vecteurs propres $X_{i}$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
