{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Matrices et transformations linéaires\n",
    "\n",
    "Les matrices apparaissent comme représentation des transformations des vecteurs. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Matrice\n",
    "\n",
    "Une [matrice](https://fr.wikipedia.org/wiki/Matrice_(math%C3%A9matiques)) $M$ est une collection à deux dimensions de nombres représentée sous la forme\n",
    "\n",
    "$$\n",
    "M_{m,n} = \n",
    "\\left(\n",
    "\\begin{array}{c,c,c,c}\n",
    "m_{1,1} & m_{1,2} & \\cdots & m_{1,n} \\\\\n",
    "m_{2,1} & m_{2,2} & \\cdots & m_{2,n} \\\\\n",
    "\\vdots  & \\vdots  & \\ddots & \\vdots  \\\\\n",
    "m_{m,1} & m_{m,2} & \\cdots & m_{m,n} \n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "ici possédant $m$ lignes et $n$ colonnes. On dit aussi que la matrice $M$ est de taille $m\\times n$.\n",
    "\n",
    "On représente souvent les matrices par leurs éléments, par exemple la notation $M\\equiv\\left(m_{i,j}\\right)_{i=1\\cdots n}^{j=1\\cdots m}$ est souvent employée (il ne faut alors pas confondre l'élément $m_{ij}$ et le nombre $m$ de colonnes de la matrice $M$). La notation $M_{m,n}$ est également courante, et permet de visualiser la taille de la matrice. Par abus de notation, on note parfois $\\left(M\\right)_{i,j}$ l'élément $\\left(i,j\\right)$ de la matrice $M$.\n",
    "\n",
    "\n",
    "```{admonition} Matrice et représentation\n",
    ":class: warning\n",
    "\n",
    "Comme pour les vecteurs, la notion de matrice $M$ est extrinsèque à tout reprère, mais la représentation sous forme de ses composantes $\\left(m_{i,j}\\right)_{i=1\\cdots n}^{j=1\\cdots m}$ n'a de sens qu'une fois donnée une base.\n",
    "\n",
    "```\n",
    "\n",
    "Dans la suite on va se concentrer sur les matrices carrées pour lesquelles $m=n$. Ces matrices représentent les transformations linéaires des vecteurs. De plus, pour les examples, on se contentera de matrices $2\\times 2$ pour simplifier.\n",
    "\n",
    "### Algèbre matricielle\n",
    "\n",
    "Détaillons les opérations de base des matrices : \n",
    "\n",
    " - l'addition (la différence, la multiplication, la division) d'une matrice par un nombre correspond à la somme (la différence, la multiplication, la division) de chacun des éléments de la matrice par le nombre : \n",
    " \n",
    "$$\n",
    "A = \n",
    "\\left(\n",
    "\\begin{array}{c,c}\n",
    " a_{1,1} & a_{1,2} \\\\\n",
    "a_{2,1} & a_{2,2}\n",
    "\\end{array}\n",
    "\\right) \\; , \\; \n",
    "\\alpha \\in \\mathbb{C}\n",
    "\\Rightarrow \n",
    "A+\\alpha = \n",
    "\\left(\n",
    "\\begin{array}{c,c}\n",
    " a_{1,1} + \\alpha & a_{1,2} + \\alpha \\\\\n",
    "a_{2,1} + \\alpha & a_{2,2} + \\alpha\n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    " - l'addition (la différence, la multiplication, la division) de deux matrices correspond à la somme (la différence, la multiplication, la division) de chacun des éléments de l'une par l'élément correspondant de l'autre : \n",
    " \n",
    "$$\n",
    "A = \n",
    "\\left(\n",
    "\\begin{array}{c,c}\n",
    " a_{1,1} & a_{1,2} \\\\\n",
    "a_{2,1} & a_{2,2}\n",
    "\\end{array}\n",
    "\\right) \\; , \\; \n",
    "B = \n",
    "\\left(\n",
    "\\begin{array}{c,c}\n",
    "b_{1,1} & b_{1,2} \\\\\n",
    "b_{2,1} & b_{2,2}\n",
    "\\end{array}\n",
    "\\right)\n",
    "\\Rightarrow \n",
    "A+B = \n",
    "\\left(\n",
    "\\begin{array}{c,c}\n",
    " a_{1,1} + b_{1,1} & a_{1,2} + b_{1,2} \\\\\n",
    "a_{2,1} + b_{2,1} & a_{2,2} + b_{2,2}\n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "Attention, [certains auteurs](https://fr.wikipedia.org/wiki/Matrice_(math%C3%A9matiques)#Addition_des_matrices_et_multiplication_par_un_scalaire) ne définissent que la somme (ou différence) de deux matrices, et la multiplication par un scalaire."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transformations linéaires\n",
    "\n",
    "Les transformations linéaires sont toutes les transformations qui donnent le nouveau vecteur $\\vec{a}^{\\prime} = a_{x}^{\\prime}\\vec{e}_{x} + a_{y}^{\\prime}\\vec{e}_{y}$ à  partir du vecteur $\\vec{a} = a_{x}\\vec{e}_{x} + a_{y}\\vec{e}_{y}$ sous la forme d'une combinaison linéaire de ses anciens coefficients \n",
    "\n",
    "\\begin{align}\n",
    "a_{x}^{\\prime} &= a_{xx} a_{x} + a_{xy} a_{y} + b_{x} \\\\\n",
    "a_{y}^{\\prime} &= a_{yx} a_{x} + a_{yy} a_{y} + b_{y}\n",
    "\\end{align}\n",
    "\n",
    "où les coeffcients $a_{ij}$ ne dépendent pas des coordonnées $x$ ou $y$ dans notre cas simple à deux dimensions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multiplication d'une matrice par un vecteur\n",
    "\n",
    "Si on représente un vecteur sous la forme d'un tableau vertical de nombres dans une base donnée\n",
    "\n",
    "$$\n",
    "\\vec{x} = \\alpha \\vec{e}_{x} + \\beta \\vec{e}_{y} =  \n",
    "\\left(\n",
    "\\begin{array}{r}\n",
    "\\alpha \\\\\n",
    "\\beta\n",
    "\\end{array}\n",
    "\\right)_{\\left\\{\\vec{e}_{x},\\vec{e}_{y}\\right\\}}\n",
    "$$\n",
    "\n",
    "alors la multiplication d'une matrice par un vecteur sera la somme des lignes de la matrices par les coefficients du vecteur : \n",
    "\n",
    "$$\n",
    "A = \n",
    "\\left(\n",
    "\\begin{array}{c,c}\n",
    " a_{xx} & a_{xy} \\\\\n",
    "a_{yx} & a_{yy}\n",
    "\\end{array}\n",
    "\\right)_{\\left\\{\\vec{e}_{x},\\vec{e}_{y}\\right\\}} \\; , \\; \n",
    "\\vec{x} =   \n",
    "\\left(\n",
    "\\begin{array}{r}\n",
    "\\alpha \\\\\n",
    "\\beta\n",
    "\\end{array}\n",
    "\\right)_{\\left\\{\\vec{e}_{x},\\vec{e}_{y}\\right\\}}\n",
    "\\Rightarrow \n",
    "A\\cdot\\vec{x} = \n",
    "\\left(\n",
    "\\begin{array}{l}\n",
    " a_{xx} \\alpha + a_{xy} \\beta \\\\\n",
    " a_{yx} \\beta + a_{yy} \\alpha\n",
    "\\end{array}\n",
    "\\right)_{\\left\\{\\vec{e}_{x},\\vec{e}_{y}\\right\\}}\n",
    "$$\n",
    "\n",
    "et c'est un nouveau vecteur, dans la même base. \n",
    "\n",
    "```{admonition} Représentation des vecteurs de la base\n",
    ":class: warning\n",
    "Notons que l'on omet très souvent d'indiquer dans quelle base on procède au calcul, i.e. on enlève l'indice $\\left\\{\\vec{e}_{x},\\vec{e}_{y}\\right\\}$ dans les représentations. C'est d'ailleurs ce que l'on va faire à l'avenir.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Représentons maintenant les vecteurs $\\vec{a}$ et $\\vec{a}^{\\prime}$ précédents sous la forme de tableaux de nombres. On a\n",
    "\n",
    "$$\n",
    "\\vec{a} =   \n",
    "\\left(\n",
    "\\begin{array}{r}\n",
    "a_{x} \\\\\n",
    "a_{y}\n",
    "\\end{array}\n",
    "\\right) \\; , \\; \n",
    "\\vec{a}^{\\prime} = \n",
    "\\left(\n",
    "\\begin{array}{r}\n",
    "a_{xx} a_{x} + a_{xy} a_{y} + b_{x} \\\\\n",
    "a_{yx} a_{x} + a_{yy} a_{y} + b_{y}\n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "On remarque alors que le vecteur $A\\cdot\\vec{x}$ correspond presque exactement à une transformation linéaire. Il nous manque néanmoins une dimension, que l'on prend égale à $1$ dans la représentation du vecteur. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Matrices de transformation\n",
    "\n",
    "Ainsi, si l'on définit le vecteur $\\vec{a}$ avec une composante de plus que le nombre de dimension de l'espace\n",
    "\n",
    "$$\n",
    "\\vec{a} =   \n",
    "\\left(\n",
    "\\begin{array}{r}\n",
    "a_{x} \\\\\n",
    "a_{y} \\\\\n",
    "1\n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "et la matrice augmentée d'une dimension\n",
    "\n",
    "$$\n",
    "A = \n",
    "\\left(\n",
    "\\begin{array}{c,c,c}\n",
    " a_{xx} & a_{xy} & b_{x} \\\\\n",
    "a_{yx} & a_{yy} & b_{y} \\\\\n",
    "0 & 0 & 1\n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "on aura, au sens de la multiplication d'une matrice par un vecteur, la quantité\n",
    "\n",
    "$$\n",
    "A\\cdot \\vec{a} = \\left(\n",
    "\\begin{array}{r}\n",
    "a_{xx} a_{x} + a_{xy} a_{y} + b_{x} \\\\\n",
    "a_{yx} a_{x} + a_{yy} a_{y} + b_{y} \\\\\n",
    "1\n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "qui représente bien le vecteur $\\vec{a}^{\\prime}$, avec la convention suivante : seules les deux première dimensions de la représentation nous intéresse ; la troisième dimension est fictive mais permet de représenter les transformations linéaires sous la forme de matrices."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "L'ensemble des [transformations linéaires](https://en.wikipedia.org/wiki/Transformation_matrix) peut donc se représenter sous la forme de matrices. On en donne quelques exemples importants sur la figure suivante.\n",
    "\n",
    "![Transformation linéaires homogènes et leur représentation matricielle canonique](fig/2D_affine_transformation_matrix.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Composition de transformation et multiplication matricielle\n",
    "\n",
    "On peut donc représenter toutes les transformations linéaires sous la forme de matrices. L'intérêt principal de cette représentation consiste à pouvoir appliquer différentes transformations à la suite. Ce phénomène, appelé composition des transformations, a une représentation très simple dans l'algèbre des matrices : la **composition de transformation est représentée par une multiplication matricielle**. \n",
    "\n",
    "```{admonition} Produit de matrices\n",
    ":class: tips\n",
    "Soit la matrice $A$ ayant $m$ lignes et $n$ colones représentée par les éléments \n",
    "\n",
    "$$\n",
    "\\begin{equation}\n",
    "A_{m,n} = \n",
    "\\begin{pmatrix}\n",
    "a_{1,1} & a_{1,2} & \\cdots & a_{1,n} \\\\\n",
    "a_{2,1} & a_{2,2} & \\cdots & a_{2,n} \\\\\n",
    "\\vdots  & \\vdots  & \\ddots & \\vdots  \\\\\n",
    "a_{m,1} & a_{m,2} & \\cdots & a_{m,n} \n",
    "\\end{pmatrix}\n",
    "\\end{equation}\n",
    "$$\n",
    "\n",
    "et la matrice $B$ de taille $n\\times p$ représentée par ses éléments $\\left(b_{n,p}\\right)$, le produit matriciel $A\\cdot B$ sera représenté par la matrice $C$ d'éléments $\\left(c_{i,j}\\right)$ pour $i\\in\\left\\{1,2,\\cdots ,m\\right\\}$ et $j\\in\\left\\{1,2,\\cdots ,p\\right\\}$ calculés comme\n",
    "\n",
    "$$\n",
    "c_{i,j} = \\sum_{k=1}^{n} a_{i,k}b_{k,j}\n",
    "$$\n",
    "\n",
    "Cela revient à faire la somme des produit des lignes de $A$ avec les colonnes de $B$. La matrice $C$ sera de taille $m \\times p$. \n",
    "```\n",
    "\n",
    "![Multiplication de deux matrices](fig/Matrix_multiplication_diagram.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ainsi, enchaîner une rotation $R_{\\theta}$ d'un angle $\\theta$ autour de l'axe $Oz$ avec une translation $T_{\\alpha,\\beta}$ de $\\alpha$ le long de l'axe $Ox$ et de $\\beta$ le long de l'axe $Oy$ s'écrira simplement $T_{\\alpha,\\beta}\\cdot R_{\\theta}$. \n",
    "\n",
    "On a\n",
    "\n",
    "$$\n",
    "R_{\\theta} = \n",
    "\\begin{pmatrix}\n",
    "\\cos\\theta & -\\sin\\theta & 0 \\\\\n",
    "\\sin\\theta & \\cos\\theta & 0 \\\\ \n",
    "0 & 0 & 1\n",
    "\\end{pmatrix} \\; , \\; \n",
    "T_{\\alpha,\\beta} = \n",
    "\\begin{pmatrix}\n",
    "1 & 0 & \\alpha \\\\\n",
    "0 & 1 & \\beta \\\\ \n",
    "0 & 0 & 1\n",
    "\\end{pmatrix}\n",
    "\\Rightarrow \n",
    "T_{\\alpha,\\beta}\\cdot R_{\\theta} = \n",
    "\\begin{pmatrix}\n",
    "\\cos\\theta & -\\sin\\theta & \\alpha \\\\\n",
    "\\sin\\theta & \\cos\\theta & \\beta \\\\ \n",
    "0 & 0 & 1\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "```{admonition} Ordre des matrices et non-commutativité du produit matriciel\n",
    ":class: danger\n",
    "Attention à l'ordre des matrices, la translation appliquée après la rotation arrive en première position dans la lecture de gauche à droite latine. Cela vient du fait que la matrice s'applique _sur la droite_. Ainsi, la translation $T_{\\alpha,\\beta}$ s'applique ici sur le vecteur $R_{\\theta}\\vec{a}$. \n",
    "\n",
    "De façon générale, **le produit matriciel ne commute pas** : $A\\cdot B \\neq B\\cdot A$.\n",
    "```\n",
    "\n",
    "À titre d'exemple, calculons\n",
    "\n",
    "$$\n",
    "R_{\\theta}\\cdot T_{\\alpha,\\beta} = \n",
    "\\begin{pmatrix}\n",
    "\\cos\\theta & -\\sin\\theta & \\alpha \\cos\\theta - \\beta \\sin\\theta \\\\\n",
    "\\sin\\theta & \\cos\\theta & \\alpha \\sin\\theta + \\beta \\cos\\theta \\\\ \n",
    "0 & 0 & 1\n",
    "\\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Représentation des matrices dans Python\n",
    "\n",
    "Python étant un langage récent, il autorise plusieurs paradigmes de programmation. On illustre la programmation procédurale et la programmation orientée objet dans les exemples ci-dessous.\n",
    "\n",
    "### Liste de listes\n",
    "\n",
    "La façon la plus simple de représenter des matrices dans Python (comme dans la quasi-totalité des langages) est d'utiliser une liste de listes. On va illustrer ci-dessous le fait que de façon générale, **le produit de matrice ne commute pas**. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "9 3 8 8 7\n",
      "7 5 5 9 3\n",
      "1 8 6 8 6\n",
      "1 9 5 8 5\n",
      "2 2 6 9 1\n",
      "---------\n",
      "4 7 8 5 1\n",
      "9 1 5 4 9\n",
      "7 1 8 9 1\n",
      "1 6 1 8 3\n",
      "7 8 6 9 7\n"
     ]
    }
   ],
   "source": [
    "from random import randint, shuffle\n",
    "\n",
    "M = [[randint(1,9) for _ in range(5)] for _ in range(5)]\n",
    "N = [[randint(1,9) for _ in range(5)] for _ in range(5)]\n",
    "\n",
    "def print_matrix(A, padding=1):\n",
    "    string = '\\n'.join(' '.join(str(i).rjust(padding) for i in line)\n",
    "                       for line in A)\n",
    "    print(string)\n",
    "    return string\n",
    "\n",
    "_ = print_matrix(M)\n",
    "print('-'*9)\n",
    "_ = print_matrix(N)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " 76  20  55  48  -6\n",
      " 23  11 -33 -35 -20\n",
      " 79 -56 -20  -1  -1\n",
      " 97 -10  29  34  72\n",
      "-64 -120 -130 -158 -101\n"
     ]
    }
   ],
   "source": [
    "def matrix_product(A,B):\n",
    "    \"\"\"Matrix Product of two matrices A and B, each being list of list\"\"\"\n",
    "    # nb_rows_B == nb_cols_A\n",
    "    assert len(B) == len(A[0])\n",
    "    C = [[0]*len(B[0]) for _ in range(len(A))]\n",
    "    for i in range(len(A)):\n",
    "        for j in range(len(B[0])):\n",
    "            C[i][j] = sum(A[i][k]*B[k][j] for k in range(len(B)))\n",
    "    return C\n",
    "\n",
    "def matrix_difference(A,B):\n",
    "    \"\"\"Difference of two matrices A and B, each being list of list\"\"\"\n",
    "    # nb_rows_B == nb_rows_A\n",
    "    assert len(B[0]) == len(A[0])\n",
    "    # nb_cols_B == nb_cols_A\n",
    "    assert len(B) == len(A)\n",
    "    C = [[0]*len(A[0]) for _ in range(len(A))]\n",
    "    for i in range(len(A)):\n",
    "        for j in range(len(B[0])):\n",
    "            C[i][j] = A[i][j] - B[i][j]\n",
    "    return C\n",
    "\n",
    "MN = matrix_product(M,N)\n",
    "NM = matrix_product(N,M)\n",
    "\n",
    "commutator = matrix_difference(MN,NM)\n",
    "_ = print_matrix(commutator, padding=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "et on peut vérifier que le commutator est impair $\\left[M, N\\right] = - \\left[N, M\\right]$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-76 -20 -55 -48   6\n",
      "-23 -11  33  35  20\n",
      "-79  56  20   1   1\n",
      "-97  10 -29 -34 -72\n",
      " 64 120 130 158 101\n"
     ]
    }
   ],
   "source": [
    "commutator = matrix_difference(NM,MN)\n",
    "_ = print_matrix(commutator, padding=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Objet appelable\n",
    "\n",
    "Dans la [programmation orientée objet](https://docs.python.org/3/tutorial/classes.html), on peut faire la part des choses entre les paramètres et les variables (entre autre). De même, un objet peut surclasser un autre, et hériter de l'ensemble de ses méthodes. On illustre cette possibilité en sur-classant la classe `TransformationMatrix`, qui présente l'opération multiplication matricielle `@` et la peut être appelé sur un vecteur (une liste simple). Comme les matrices de transformation présentent une dimension supplémentaire que le vecteur, appeler une instance de `TransformationMatrix` sur une liste doit d'abord augmenter la taille de cette liste, puis retourner le produit matriciel modifié poru teir compte de la nature de cette liste.\n",
    "\n",
    "Comme l'essentiel des méthodes instanciées ci-dessous sont des [dunder/magic methods](https://docs.python.org/3/reference/datamodel.html#special-method-names) de Python plus l'[opérateur `__matmul__`](https://docs.python.org/3/library/operator.html), on ne les détaille pas ici."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import List\n",
    "\n",
    "class TransformationMatrix(object):\n",
    "    \n",
    "    def __init__(self, array: List[List[float]]):\n",
    "        try:\n",
    "            if len(array) != len(array[0]):\n",
    "                raise ValueError(\"Must be a square matrix\")\n",
    "        except IndexError:\n",
    "            raise ValueError(\"Must represent a matrix\")\n",
    "        self.matrix = array\n",
    "        return None\n",
    "    \n",
    "    def __len__(self):\n",
    "        return len(self.matrix)\n",
    "    \n",
    "    def __getitem__(self, position):\n",
    "        i,j = position\n",
    "        return self.matrix[i][j]\n",
    "\n",
    "    def __setitem__(self, position, value):\n",
    "        i,j = position\n",
    "        self.matrix[i][j] = value\n",
    "        return None\n",
    "    \n",
    "    def __repr__(self):\n",
    "        return \"\\n\".join(\"\\t\".join(str(round(_,2)) for _ in line)\n",
    "                         for line in self.matrix)\n",
    "    \n",
    "    @property\n",
    "    def T(self):\n",
    "        matrix = self.__class__(self.matrix)\n",
    "        for i in range(len(self)):\n",
    "            for j in range(i+1, len(self)):\n",
    "                matrix[i,j] = self[j,i]\n",
    "        return matrix\n",
    "        \n",
    "    def __matmul__(self, matrix):\n",
    "        if len(matrix) != len(self):\n",
    "            raise ValueError(\"matrices must be compatible\")\n",
    "        mat_product = TransformationMatrix([[0]*len(self) \n",
    "                                            for _ in range(len(self))])\n",
    "        for i in range(len(self)):\n",
    "            for j in range(len(self)):\n",
    "                mat_product[i,j] = sum(self[i,k]*matrix[k,j] \n",
    "                                       for k in range(len(self)))\n",
    "        return mat_product\n",
    "    \n",
    "    def __call__(self, vector: List[float]):\n",
    "        if len(vector) != len(self)-1:\n",
    "            raise ValueError\n",
    "        vec = vector.copy() + [1]\n",
    "        return [sum(self[i,j] for j in range(len(vec)))\n",
    "                for i in range(len(vec))][:-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notons toutefois que les méthodes `__setitem__` et `__getitem__` permettent de récupérer et d'écrire les éléments d'une matrices à l'aide de l'écriture `[i,j]` en place de `[i][j]` avec une liste de listes. Cette écriture peut paraître plus souple, ou plus naturelle.\n",
    "\n",
    "Une fois la classe mère implémentée, on peut la sur-classer pour générer les cas particuliers des translations et des rotations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import cos, sin, pi\n",
    "\n",
    "class Translation(TransformationMatrix):\n",
    "    \n",
    "    def __init__(self, x: float=0.0, y: float=0.0):\n",
    "        array = [[1,0,x],[0,1,y],[0,0,1]]\n",
    "        super().__init__(array)\n",
    "        return None\n",
    "\n",
    "class Rotation(TransformationMatrix):\n",
    "    \n",
    "    def __init__(self, angle: float=0.0, dimension: str='rad'):\n",
    "        allowed_dimensions = ['degree', '°', 'rad', 'radian']\n",
    "        if str(dimension) not in allowed_dimensions:\n",
    "            mess = f\"dimension must be in {allowed_dimensions}, \"\n",
    "            mess += f\"receive {dimension}\"\n",
    "            raise ValueError(mess)\n",
    "        if str(dimension) in allowed_dimensions[:2]:\n",
    "            angle = angle*pi/180\n",
    "        array = [[cos(angle), -sin(angle), 0], \n",
    "                 [sin(angle), cos(angle), 0], \n",
    "                 [0,0,1]]\n",
    "        super().__init__(array)\n",
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On peut ainsi facilement montrer que le produit matriciel ne commute pas en général : appliquer une rotation puis une translation ne revient pas toujours à appliquer d'abord la translation puis la rotation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.0\t-1.0\t-5.0\n",
      "1.0\t0.0\t2.0\n",
      "0\t0\t1\n",
      "--------------------\n",
      "0.0\t-1.0\t2\n",
      "1.0\t0.0\t5\n",
      "0.0\t0.0\t1\n"
     ]
    }
   ],
   "source": [
    "r = Rotation(pi/2)\n",
    "t = Translation(2,5)\n",
    "print(r@t)\n",
    "print(\"-\"*20)\n",
    "print(t@r)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Illustrons finalement la transformation d'un vecteur"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[3, 6]"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "t([1,3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[-0.9999999999999999, 1.0]"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "r([1,3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1.0, 6.0]"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(t@r)([1,3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[-6.0, 3.0000000000000004]"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(r@t)([1,3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "et faisons attention à ne pas confondre l'appel de la classe sur le vecteur `[1,2]` `r([1,2])` de la récupération de l'élément `[1,2]` de l'objet `r[1,2]`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[-0.9999999999999999, 1.0]"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "r([1,2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "r[1,2]"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
