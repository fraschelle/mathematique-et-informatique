{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Système d'équations linéaires\n",
    "\n",
    "Un [système d'équations linéaires](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27%C3%A9quations_lin%C3%A9aires) est un ensemble d'équations ayant plusieurs inconnues $x_1$, $x_2$, $x_i$, ..., $x_n$ liées entre elles par des expressions ne contenant pas de degrés supérieur à $1$ (d'où le nom de linéaire).\n",
    "\n",
    "Ces systèmes se calculent soit en inversant la matrice correspondante, soit en appliquant la [méthode du pivot de Gauss](https://fr.wikipedia.org/wiki/%C3%89limination_de_Gauss-Jordan), qui consiste à produire une représentation triangulaire de la matrice."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Système d'équations et représentation matricielle\n",
    "\n",
    "Un système de $m$ équations à $n$ inconnues se met généralement sous la forme\n",
    "\n",
    "$$\n",
    "\\left\\{\\begin{matrix}  a_{1,1}x_1+a_{1,2}x_2+\\dots+a_{1,n}x_n = b_1 \\\\ a_{2,1}x_1+a_{2,2}x_2+\\dots+a_{2,n}x_n = b_2 \\\\ \\vdots \\\\ \\vdots \\\\ a_{m,1}x_1+a_{m,2}x_2+\\dots+a_{m,n}x_n = b_m\\end{matrix}\\right.\n",
    "$$\n",
    "\n",
    "et si l'on isole les inconnues $x_{i} \\in \\left\\{x_{1}, x_{2}, \\cdots, x_{n}\\right\\}$ des variables $a_{i,j}$ et $b_{i}$ on peut mettre ce système sous la forme d'une équation matricielle $A\\cdot x = b$, avec \n",
    "\n",
    "$$\n",
    "A=\\begin{pmatrix}\n",
    "a_{1,1} & a_{1,2} & \\cdots & a_{1,n} \\\\\n",
    "a_{2,1} & a_{2,2} & \\cdots & a_{2,n} \\\\\n",
    "\\vdots & \\vdots & \\ddots & \\vdots \\\\\n",
    "a_{m,1} & a_{m,2} & \\cdots & a_{m,n} \\end{pmatrix}; \\qquad x=\\begin{pmatrix} x_1 \\\\ x_2\\\\ \\vdots \\\\ x_n \\end{pmatrix}\\quad\\text{et}\\quad b=\\begin{pmatrix} b_1 \\\\ b_2 \\\\ \\vdots \\\\ b_m \\end{pmatrix}\n",
    "$$\n",
    "\n",
    "puisque chaque ligne s'écrit sous la forme d'un produit matriciel (la seconde matrice étant une matrice colonne) $b_{i} = \\sum_{j=1}^{n} a_{i,j}x_{j}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Système homogène\n",
    ":class: note\n",
    "Un système de la forme $Ax=0$ est appelé **système d'équations linéaires homogènes** (ou sans **second membre**). Tous les systèmes homogènes admettent au moins une solution $x_1=0 \\ ; \\ x_2=0 \\ ; \\ \\dots \\ ; \\ x_n=0$, appelée solution **nulle** ou **triviale**.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solution d'un système d'équations linéaires\n",
    "\n",
    "Trouver la solution du système d'équations $A\\cdot x = b$ revient à calculer l'[inverse de la matrice](https://fr.wikipedia.org/wiki/Matrice_inversible) $A$ : $x = A^{-1}\\cdot b$.\n",
    "\n",
    "Un système d'équations linéaires présente : \n",
    "\n",
    " - aucune solution\n",
    " - une seule solution\n",
    " - une infinité de solutions\n",
    "\n",
    "En particulier, \n",
    "\n",
    " - il n'aura aucune solution s'il présente plus d'équations que d'inconnues\n",
    " - il aura une infinité de solutions s'il présente moins d'équations que d'inconnues\n",
    " \n",
    "On se place dans la suite dans le cas où nombre d'équations et d'inconnues sont identiques.\n",
    "\n",
    "Un système d'équations linéaires ayant le même nombre $n$ d'inconnues que d'équations est représenté par une matrice carrée $n\\times n$. Une solution explicite existe dans ce cas là, sous la forme dite de la [règle de Cramer](https://fr.wikipedia.org/wiki/R%C3%A8gle_de_Cramer) : \n",
    "\n",
    "$$\n",
    "x_{k} = { \\det(A_{k}) \\over \\det(A) }\n",
    "$$\n",
    "\n",
    "où $A_{k}$ est la matrice $n\\times n$ obtenue en remplaçant la $k$-ième colonne par le vecteur $b$ : \n",
    "\n",
    "$$\n",
    "A_k = ( a_{k|i,j} ) \\mbox{ avec } a_{k|i,j} = \\left\\{\\begin{matrix} a_{i,j} & \\mbox{si } j \\ne k \\\\ b_{i} & \\mbox{si }j = k\\end{matrix}\\right.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Nombre de solutions d'un système d'équations linéaires\n",
    ":class: danger\n",
    "Le système d'équations n'aura donc pas de solution au sens de Cramer lorsque $\\det(A)=0$. C'est en particulier le cas lorsque le système n'est pas indépendant : certaines équations se déduisent des autres par multiplication. On dit dans ce cas que les équations sont liénairement dépendantes. Il en résulte que certaines lignes de la matrice $A$ se déduisent d'une autre par composition linéaire (addition de plusieurs lignes multipliée par un coefficient).\n",
    "\n",
    "Toutefois, le système peut quand même présenter une solution dans le cas $\\det(A)=0$ : il faut se référer au [théorème de Rouché-Fontené](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Rouch%C3%A9-Fonten%C3%A9) pour connaître le nombre de solutions d'un système d'équations linéaires.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Méthode du pivot de Gauss\n",
    "\n",
    "Une méthode de résolution plus directe (et moins coûteuse en terme d'algorithme) consiste à transformer la matrice carrée $n\\times n$ en une matrice triangulaire (supérieure ou inférieure) $T$. En effet, une matrice triangulaire supérieure est de la forme\n",
    "\n",
    "$$\n",
    "T=\\begin{pmatrix}\n",
    "t_{1,1} & t_{1,2} & t_{1,3} & \\cdots & t_{1,n} \\\\\n",
    "0 & t_{2,2} & t_{2,3} & \\cdots & t_{2,n} \\\\\n",
    "0 & 0 & t_{3,3} & \\cdots & t_{3,n} \\\\\n",
    "\\vdots & \\vdots & \\ddots & \\ddots \\\\\n",
    "0 & 0 & \\cdots & 0 & t_{n,n} \\end{pmatrix}\n",
    "$$\n",
    "\n",
    "et l'équation $T\\cdot x = c$ se résout alors facilement, en commençant par la dernière équation : \n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "x_n &= \\frac{c_n}{t_{n,n}} \\\\\n",
    "x_{n-1} &= \\frac{c_{n-1}-t_{n-1,n}x_n}{t_{n-1,n-1}} \\\\\n",
    "x_{n-2} &= \\frac{c_{n-2}-t_{n-2,n} x_{n}-t_{n-2,n-1}x_{n-1}}{t_{n-2,n-2}} \\\\\n",
    "\\vdots\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "le terme générique étant\n",
    "\n",
    "$$\n",
    "x_{k} = \\frac{1}{t_{k,k}}\\left(c_{k}-\\sum_{j=k-1}^{n}t_{k,j}x_{j}\\right)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or, mettre la matrice carrée $A$ sous une forme triangulaire se fait par l'[élimination de Gauss-Jordan](https://fr.wikipedia.org/wiki/%C3%89limination_de_Gauss-Jordan). Il suffit de substituer la ligne $L_{i}$ par une superposition linéaire d'une ligne dite _pivot_ $L_{p}$ et de la ligne $L_{i}$. Il faut également appliquer la même substitution sur le vecteur $b$. \n",
    "\n",
    "On construit donc tout d'abord la matrice dite _augmentée_\n",
    "\n",
    "$$\n",
    "A=\\begin{pmatrix}\n",
    "a_{1,1} & a_{1,2} & \\cdots & a_{1,n} & b_{1} \\\\\n",
    "a_{2,1} & a_{2,2} & \\cdots & a_{2,n} & b_{2} \\\\\n",
    "\\vdots & \\vdots & \\ddots & \\vdots & \\vdots \\\\\n",
    "a_{n,1} & a_{n,2} & \\cdots & a_{n,n} & b_{n} \\end{pmatrix}\n",
    "$$\n",
    "\n",
    "On procède de haut en bas. La première ligne pivot est donc $L_{1}$. On remplace alors $L_{2}$ par $a_{1,1}L_{2} - a_{2,1}L_{1}$, ce qui aura pour effet d'éliminer le premier coefficient de la nouvelle ligne $L_{1}$. On procède de même pour l'ensemble des lignes de sorte que l'on aura\n",
    "\n",
    "$$\n",
    "A^{\\prime}=\\begin{pmatrix}\n",
    "a_{1,1} & a_{1,2} & \\cdots & a_{1,n} & b_{1} \\\\\n",
    "0 & a_{1,1}a_{2,2}-a_{2,1}a_{1,2} & \\cdots & a_{1,1}a_{2,n}-a_{2,1}a_{1,n} & a_{1,1}b_{2}-a_{2,1}b_{1} \\\\\n",
    "\\vdots & \\vdots & \\ddots & \\vdots & \\vdots \\\\\n",
    "0 & a_{1,1}a_{n,2}-a_{n,1}a_{1,2}& \\cdots & a_{1,1}a_{n,n}-a_{n,1}a_{1,n} & a_{1,1}b_{n}-a_{n,1}b_{1}\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "On procède ensuite de la même façon, mais en recommençant avec la sous-matrice $A_{1\\cdots n, 1\\cdots n}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Pivot de Gauss et stabilité numérique\n",
    ":class: attention\n",
    "Dans la pratique, on préfère faire la substitution $L_{s} \\rightarrow L_{s} - \\frac{a_{s,p}L_{p}}{a_{p,p}}$ ($L_{p}$ est la ligne pivot, $L_{s}$ la ligne substituée),  qui permet de travailler avec des réels. Il faut alors prendre garde à la stabilité numérique des multiples substitutions.\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
