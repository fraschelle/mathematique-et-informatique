# -*- coding: utf-8 -*-
"""
Series of some known functions.
"""

from typing import Dict
from fractions import Fraction

class PascalTriangle(list):
    """Sequence that constructs the PascalTriangle."""
    def __init__(self):
        super().__init__([[1]])
        return None
    def __next__(self):
        line = [0] + self[-1] + [0]
        line = [x + y for x, y in zip(line[:-1], line[1:])]
        self.append(line)
        return self[-1]
    def __getitem__(self, n: int):
        while len(self) < n+1:
            next(self)
        return super().__getitem__(n)

class BernouilliNumber(list):
    """Bernouilli number, in the form of a list."""
    def __init__(self):
        super().__init__([Fraction(1,1), Fraction(-1,2)])
        self.cmk = PascalTriangle()
        return None
    def __next__(self):
        if len(self)%2:
            self.append(Fraction(0,1))
            return self[-1]
        numer = sum(self[i] * self.cmk[len(self)+1][i]
                    for i in range(len(self)))
        self.append(Fraction(-numer, len(self)+1))
        return self[-1]
    def __getitem__(self, n: int):
        while len(self) < n+1:
            next(self)
        return super().__getitem__(n)

def EulerEEval(precision=0.000001) -> float:
    """Numerial evaluation of Euler constant."""
    pres, q, factorial, euler = 1, 1, 1, 1
    # pres can be negative, so use the square
    while pres > precision:
        factorial *= q
        pres = 1/factorial
        euler += pres
        q += 1
    return euler

class Exp():
    """Evaluation of the exponential function by ots Taylor development."""
    def __init__(self, precision: float = 0.000001):
        self.precision = float(precision)
        self.E = EulerEEval(precision=self.precision)
        return None
    def __call__(self, x):
        """Numerial evaluation of the exponential function."""
        power = int(x)
        rest = x - power
        precision, q, factorial, extraexp = 1, 1, 1, 1
        while abs(precision) > self.precision:
            factorial *= q
            precision *= rest
            extraexp += precision/factorial
            q += 1
        return self.E**power * extraexp
    def polynom(self, order=4) -> Dict[int, Fraction]:
        """Polynomial of the series of the exponential function."""
        q, poly, denom = 1, {0: 1}, 1
        while q <= order:
            denom *= q
            poly[q] = Fraction(1, denom)
            q += 1
        return poly

class Cosine():
    """Evaluation of the cosine function by its Taylor development.
    
    see https://en.wikipedia.org/wiki/Trigonometric_functions#Power_series_expansion
    """
    def __init__(self, precision: float = 0.000001):
        self.precision = float(precision)
        return None
    def __call__(self, x):
        """Numerial evaluation of the cosine function."""
        res, precision, numer, q, denom = 1, 1, 1, 1, 1
        while precision > self.precision:
            numer *= x*x
            denom *= 2*q*(2*q-1)
            precision = numer / denom
            res += (-1 if q%2 else 1) * precision
            q += 1
        return res
    def polynom(self, order=4) -> Dict[int, Fraction]:
        """Polynomial of the series of the cosine function."""
        q, poly, denom = 1, {0: 1}, 1
        while 2*q <= order:
            numer = -1 if q%2 else 1
            denom *= 2*q*(2*q-1)
            poly[2*q] = Fraction(numer, denom)
            q += 1
        return poly

class Sine():
    """Evaluation of the cosine function by its Taylor development.
    
    see https://en.wikipedia.org/wiki/Trigonometric_functions#Power_series_expansion
    """
    def __init__(self, precision: float = 0.000001):
        self.precision = float(precision)
        return None
    def __call__(self, x):
        """Numerial evaluation of the sine function."""
        res, precision, numer, q, denom = x, abs(x), x, 1, 1
        while precision > self.precision:
            numer *= x*x
            denom *= 2*q*(2*q+1)
            precision = numer / denom
            res += (-1 if q%2 else 1) * precision
            q += 1
        return res
    def polynom(self, order=4) -> Dict[int, Fraction]:
        """Polynomial of the series of the sine function."""
        q, poly, denom = 1, {1: 1}, 1
        while 2*q+1 <= order:
            numer = -1 if q%2 else 1
            denom *= 2*q*(2*q+1)
            poly[2*q+1] = Fraction(numer, denom)
            q += 1
        return poly
