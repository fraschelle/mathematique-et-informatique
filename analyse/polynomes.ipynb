{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2572be1f",
   "metadata": {},
   "source": [
    "# Polynômes\n",
    "\n",
    "Les polynômes sont des fonctions particulières s'écrivant comme des séries de monômes. Un mônome étant une élévation à la puissance de la fonction affine. \n",
    "\n",
    "La fonction affine s'écrit $f\\left(x\\right)=ax+b$ avec $a$ et $b$ des nombres réels. Son élévation à la puissance $n$ s'écrit alors $f^{n} = a^n x^n + na^{n-1}b x^{n-1} + \\cdots + b^n$ on peut représenter un polynôme d'ordre $n$ sous la forme de la série\n",
    "\n",
    "$$\n",
    "P_{n}\\left(x\\right) = a_{n}x^{n} + a_{n-1}x^{n-1} + \\cdots + a_{0}\n",
    "$$\n",
    "\n",
    "Les polynômes font la jonction entre l'[algèbre](https://fr.wikipedia.org/wiki/Polyn%C3%B4me) et l'[analyse](https://fr.wikipedia.org/wiki/Fonction_polynomiale), où on les appelle plutôt _fonction polynomiale_. Ici, on ne s'attache pas à la distinction de nomenclature et on traite à la fois l'algèbre polynomiale (addition, soustraction, multiplication et division des polynômes) et l'analyse.\n",
    "\n",
    "L'**ordre** d'un polynôme est défini comme son degré le plus haut."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "d25026e9",
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [],
   "source": [
    "from fractions import Fraction\n",
    "from math import factorial\n",
    "\n",
    "from polynomial import Polynomial, divide_series"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ab2e83c",
   "metadata": {},
   "source": [
    "## Algèbre polynomiale\n",
    "\n",
    "Voyons comment se comporte un polynôme pour les quatres opérations de base : addition, soustraction, multiplication et division.\n",
    "\n",
    "Dans tous les exemples à venir on prend deux polynômes $p_A$ et $p_B$ d'ordre respectif $d_A$ et $d_B$\n",
    "\n",
    "$$p_A\\left(x\\right) = a_0x^0 + a_1 x^1 + a_2 x^2 + a_3 x^3 + \\cdots = \\sum_{n=0}^{d_A}a_n x^n$$\n",
    "\n",
    "$$p_B\\left(x\\right) = b_0x^0 + b_1 x^1 + b_2 x^2 + b_3 x^3 + \\cdots = \\sum_{n=0}^{d_B}b_n x^n$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9720ea5",
   "metadata": {},
   "source": [
    "### Addition et soustraction polynomiale\n",
    "\n",
    "La somme de deux polynômes est un polynôme de degré égal au maximum des degrés des deux polynômes et dont les coefficients correspondent à la somme des cofficients de même degré. Dit explicitement\n",
    "\n",
    "$$p_C\\left(x\\right) = p_A + p_B = \\sum_{n=0}^{\\max\\left(d_A, d_B\\right)}c_nx^n$$ \n",
    "\n",
    "où les coefficients $c_n$ vérifient $c_n = a_n + b_n$ pour chaque $n$. \n",
    "\n",
    "Pour la soustraction de deux polynômes, on remplace l'addition $+$ par la soustraction."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7abade29",
   "metadata": {},
   "source": [
    "### Multiplication polynomiale\n",
    "\n",
    "Le produit de deux polynômes est un polynôme de degré égal à la somme des degré des deux polynômes et dont les coefficients sont les sommes des produits deux-à-deux des coefficients dont la somme des degrés correspond au degré du monôme dans le polynôme produit. Dit explicitement\n",
    "\n",
    "$$p_C\\left(x\\right) = p_A\\times p_B = \\sum_{n=0}^{d_A+d_B}c_nx^n$$ \n",
    "\n",
    "où les coefficients $c_n$ vérifient \n",
    "\n",
    "$$c_n = a_0 b_n + a_1 b_{n-1} + a_2 b_{n-2} + \\cdots + a_n b_0 = \\sum_{k=0}^{n}a_{k} b_{n-k}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1de193b6",
   "metadata": {},
   "source": [
    "### Division polynomiale\n",
    "\n",
    "[Diviser un polynôme](https://fr.wikipedia.org/wiki/Division_d%27un_polyn%C3%B4me) $p_A$ de degré $d_A$ par un polynôme $p_B$ de degré $d_B$, c'est trouver les polynômes $Q$ et $R$ (appelé respectivement quotient et reste) tels que \n",
    "\n",
    "$$\n",
    "p_A = Qp_B + R\n",
    "$$\n",
    "\n",
    "de sorte que le degré de $R$ soit plus petit que $d_B$. Le [théorème de la division euclidienne](https://fr.wikipedia.org/wiki/Division_d%27un_polyn%C3%B4me#Th%C3%A9or%C3%A8me_et_d%C3%A9finitions) assure que les polynômes $Q$ et $R$ sont uniques une fois les polynômes $p_A$ et $p_B$ donnés."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd254470",
   "metadata": {},
   "source": [
    "```{admonition} Division euclidienne des nombres\n",
    ":class: hint\n",
    "Rappelons que deux nombres entiers $a$ et $b$ sont divisibles s'il existe deux autre nombres $q$ et $r$ (appelés respectivement quotient et reste) tels que $a = qb+r$ de sorte que $r$ soit plus petit que $b$.\n",
    "\n",
    "Pour les `int` dans Python, rappelons que $q$ s'obtient à l'aide de la division entière `//` tandis que $r$ s'obtient par l'opération module `%`, cf [Algèbre/Les nombres et leurs représentations](../algebre/nombres.html).\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f34b173",
   "metadata": {},
   "source": [
    "```{admonition} Division, entier, irrationnel et réel\n",
    ":class: danger\n",
    "La division classique, qui a deux entiers pourrait faire correspondre un nombre réel, n'existe pas pour les polynômes. Ainsi, les polynômes \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5aa442ec",
   "metadata": {},
   "source": [
    "## Représentation informatique des polynômes\n",
    "\n",
    "Il existe de nombreuses possibilités pour représenter les polynômes. On peut par exemple les représenter sous forme de liste, dont la position indique le degré. On peut encore les représenter par une application liant le degré au coefficient de ce degré, auquel cas la représentation en Python la plus proche sera un dictionnaire.\n",
    "\n",
    "Ainsi, le polynôme $2 + 4x + 7x^{3} - 2x^{5}$ pourrait avoir pour représentation en Python\n",
    "\n",
    " - la liste `[2, 4, 0, 7, 0, -2]`\n",
    " - le dictionnaire `{0: 2, 1: 4, 3: 7, 5: -1}`\n",
    "\n",
    "On remarque que \n",
    " \n",
    " - il faut donner les coefficients nuls dans la liste pour ne pas se tromper sur le degré du monôme correspondant\n",
    " - on ne peut pas avoir de degré négatif dans le dictionnaire (alors qu'il est facile d'en donner à priori)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "180a4022",
   "metadata": {},
   "source": [
    "Dans le module [polynomial](polynomial.html), on donne une représentation d'un polynôme sous la forme d'une classe surclassant un dictionnaire. Les [_dundle methods_ utilisées](https://docs.python.org/3/reference/datamodel.html#emulating-numeric-types) sont celles des opérations algébriques. On utilise de plus la méthode [get](https://docs.python.org/3/library/stdtypes.html#dict.get) de l'objet `dict`. Enfin, on utilise la [propriété](https://docs.python.org/3/library/functions.html#property) `degree` pour désigner le degré du polynôme."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd4bdd7e",
   "metadata": {},
   "source": [
    "Voyons comment utiliser cette classe. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "5da82fb6",
   "metadata": {},
   "outputs": [],
   "source": [
    "p1 = Polynomial({0: 2, 1: 4, 3: 7, 5: -1})\n",
    "p2 = Polynomial({0: 1, 1: 2, 2: 3, 3: 1, 5: 1})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42a1787c",
   "metadata": {},
   "source": [
    "L'addition tout d'abord"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "07e63bf9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(-x^5 + 7x^3 + 4x + 2) + (x^5 + x^3 + 3x^2 + 2x + 1) = 8x^3 + 3x^2 + 6x + 3\n"
     ]
    }
   ],
   "source": [
    "p = p1 + p2\n",
    "print(f\"({p1}) + ({p2}) = {p}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db9a2010",
   "metadata": {},
   "source": [
    "On vérifie ensuite que la soustraction polynomiale est bien une opération impaire : $p_A - p_B = - \\left(p_B - p_A\\right)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "4caf96ee",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(-x^5 + 7x^3 + 4x + 2) - (x^5 + x^3 + 3x^2 + 2x + 1) = -2x^5 + 6x^3 - 3x^2 + 2x + 1\n",
      "(x^5 + x^3 + 3x^2 + 2x + 1) - (-x^5 + 7x^3 + 4x + 2) = 2x^5 - 6x^3 + 3x^2 - 2x - 1\n"
     ]
    }
   ],
   "source": [
    "print(f\"({p1}) - ({p2}) = {p1-p2}\")\n",
    "print(f\"({p2}) - ({p1}) = {p2-p1}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2e3407c",
   "metadata": {},
   "source": [
    "On calcule maintenant la multiplication de ces deux polynômes. On se rend compte à quel point cette opération semblerait complexe si on avait du l'implémenter à la main"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "262d5194",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(x^5 + x^3 + 3x^2 + 2x + 1) x (-x^5 + 7x^3 + 4x + 2) = -x^10 + 6x^8 - 3x^7 + 9x^6 + 22x^5 + 18x^4 + 21x^3 + 14x^2 + 8x + 2\n"
     ]
    }
   ],
   "source": [
    "\n",
    "print(f\"({p2}) x ({p1}) = {p2*p1}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "757ded23",
   "metadata": {},
   "source": [
    "Et finalement la division polynomiale "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "f2ef747d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x^3 - x^2 + 1 = (x^2)(x - 1) + 1\n"
     ]
    }
   ],
   "source": [
    "A = Polynomial({0:1, 2:-1, 3:1})\n",
    "B = Polynomial({2:1})\n",
    "\n",
    "Q, R = divmod(A, B)\n",
    "print(f\"{A} = ({B})({Q}) + {R}\")\n",
    "assert(B * Q + R == A)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "505cd0ba",
   "metadata": {},
   "source": [
    "Notons les opérations plus élémentaires de division à seuil `A // B` et de reste (ou module) de division `A % B` présents dans l'opération `divmod(A, B)` ci-dessus."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "952b585a",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert(Q == A // B)\n",
    "assert(R == A % B)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6334811",
   "metadata": {},
   "source": [
    "Dernier exemple, extrait de [l'exemple Wikipedia sur la division polynomiale](https://fr.wikipedia.org/wiki/Division_d%27un_polyn%C3%B4me#Exemple_et_algorithme)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "26c583d3",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x^5 - x^4 - x^3 + 3x^2 - 2x = (x^2 - x + 1)(x^3 - 2x + 1) + (x - 1)\n"
     ]
    }
   ],
   "source": [
    "A = Polynomial({5:1, 4:-1, 3:-1, 2:3, 1:-2})\n",
    "B = Polynomial({2:1, 1:-1, 0:1})\n",
    "\n",
    "Q, R = divmod(A, B)\n",
    "print(f\"{A} = ({B})({Q}) + ({R})\")\n",
    "assert(B * Q + R == A)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "209e90b8",
   "metadata": {},
   "source": [
    "## Polynôme et séries\n",
    "\n",
    "Avoir implémenté l'algébre des polynôme permet de calculer les coefficients de [séries](series_developpements_limites) complexes à partir des séries simples.\n",
    "\n",
    "C'est le cas du développement limité de la tangente hyperbolique, obtenu par division du développement limité du sinus hyperbolique par le cosinus hyperbolique. Ces deux séries sont elles même obtenues par addition ou soustraction du développement limité de l'exponentielle $e^x$ et $e^{-x}$ : \n",
    "\n",
    " - $\\tanh x = \\frac{\\sinh x}{\\cosh x}$\n",
    " - $\\sinh x = \\frac{e^x - e^{-x}}{2}$\n",
    " - $\\cosh x = \\frac{e^x + e^{-x}}{2}$\n",
    "\n",
    "On peut calculer les coefficients de ces séries à l'ordre souhaité assez facilement à l'aide de notre classe. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "1a685dbd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "cosh x = 1/40320x^8 + 1/720x^6 + 1/24x^4 + 1/2x^2 + 1\n"
     ]
    }
   ],
   "source": [
    "exp = Polynomial({n:Fraction(1, factorial(n)) \n",
    "                  for n in range(10)})\n",
    "exp_ = Polynomial({n: Fraction(pow(-1, n), factorial(n))\n",
    "                   for n in range(10)})\n",
    "cosh = (exp + exp_)/2\n",
    "print(f\"cosh x = {cosh}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "8ad3581e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sinh x = 1/362880x^9 + 1/5040x^7 + 1/120x^5 + 1/6x^3 + x\n"
     ]
    }
   ],
   "source": [
    "sinh = (exp - exp_)/2\n",
    "print(f\"sinh x = {sinh}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1d0c09b5",
   "metadata": {},
   "source": [
    "Maintenant, pour construire la division polynomiale, on ne part plus des coefficient de plus haut degrés, mais des coefficients de plus faibles degrés, et on augmente la précision de la série recherchée petit à petit. Il en résulte une procédure algorithmique qui ne correspond plus à la division polynomiale euclidienne, puisque le reste sera de plus haut degré que les polynômes initiaux. On n'a donc plus de notion de factorisation des polynômes comme précédemment, mais de recherche de précision étape par étape."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "deba296e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Polynomial(-17/315x^7 + 2/15x^5 - 1/3x^3 + x)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tanh, reste = divide_series(sinh, cosh)\n",
    "tanh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c7a484b",
   "metadata": {},
   "source": [
    "Attention, l'algorithme ci-dessus n'est pas celui de la division polynomiale euclidienne. En effet, le reste de la division ci-dessus est de degré plus grand que `sinh` ou `cosh`. Par contre, on vérifiera toujours que `tanh * cosh + reste == sinh` au sens de la division polynomiale."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "90a00305",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert(tanh * cosh + reste == sinh)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
