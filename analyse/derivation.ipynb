{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dérivation\n",
    "\n",
    "La dérivation est la base de l'analyse. Calculer une dérivée consiste à calculer la limite d'une fonction avec elle même, en deux points très proches."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dérivée d'une fonction d'une seule variable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Définition de la dérivée\n",
    "\n",
    "La dérivée d'une fonction $f$ se défini comme, au choix\n",
    "\n",
    "$$\\begin{align}\n",
    "\\left.\\frac{df}{dx}\\right|_{x=x_{0}}&=\\lim_{\\epsilon\\rightarrow0}\\frac{f\\left(x_{0}+\\epsilon\\right)-f\\left(x_{0}\\right)}{\\epsilon}\n",
    "\\\\&=\\lim_{\\epsilon\\rightarrow0}\\frac{f\\left(x_{0}\\right)-f\\left(x_{0}-\\epsilon\\right)}{\\epsilon}\n",
    "\\\\&=\\lim_{\\epsilon\\rightarrow0}\\frac{f\\left(x_{0}+\\epsilon\\right)-f\\left(x_{0}-\\epsilon\\right)}{2\\epsilon}\n",
    "\\end{align}$$\n",
    "\n",
    "et ces trois définitions sont équivalentes. Elles sont appelées **définitions de Newton des dérivées**.\n",
    "\n",
    "Attention : \n",
    " \n",
    " - la dérivée d'une fonction n'existe pas toujours, et elle n'a pas nécessairement le même domaine de définition que la fonction elle même\n",
    " - la dérivée d'une fonction est une fonction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Quelques dérivées\n",
    "\n",
    " - $\\left(e^x\\right)^\\prime = e^x$\n",
    " - $\\left(x^{\\alpha}\\right)^\\prime = \\alpha x^ {\\alpha -1}$\n",
    " - $\\left(\\cos x\\right)^\\prime = -\\sin x$\n",
    " - $\\left(\\sin x\\right)^\\prime = \\cos x$\n",
    "\n",
    "Les [dérivées des fonctions trigonométriques](https://en.wikipedia.org/wiki/Differentiation_of_trigonometric_functions) utilisent leurs identités remarquables et sont donc un peu subtiles. On laisse le lecteur explorer ces démonstrations. On démontre les deux autres dérivées ci-dessous."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Dérivée de l'exponentielle\n",
    ":class: dropdown\n",
    "La démonstration de la dérivée de l'exponentielle est intéressant, puisqu'elle fait intervenir la série définissant la fonction exponentielle\n",
    "\n",
    "$$\\begin{align}\n",
    "\\lim_{\\epsilon\\rightarrow0}\\frac{e^{x+\\epsilon}-e^{x}}{\\epsilon}&=e^{x}\\lim_{\\epsilon\\rightarrow0}\\frac{e^{\\epsilon}-1}{\\epsilon}\\\\&=e^{x}\\lim_{\\epsilon\\rightarrow0}\\frac{1}{\\epsilon}\\left(\\sum_{k=0}^{\\infty}\\frac{\\epsilon^{k}}{k!}-1\\right)\\\\&=e^{x}\\lim_{\\epsilon\\rightarrow0}\\sum_{k=1}^{\\infty}\\frac{\\epsilon^{k-1}}{k!}\\\\&=e^{x}\\lim_{\\epsilon\\rightarrow0}\\left(1+\\sum_{k=0}^{\\infty}\\frac{\\epsilon^{k+1}}{\\left(k+2\\right)!}\\right)=e^{x}\n",
    "\\end{align}$$\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Calcul des dérivées\n",
    "\n",
    "Les dérivées vérifient quelques règles de calculs : \n",
    "\n",
    " - la linéarité\n",
    "\n",
    "$$\n",
    "\\frac{d}{dx}\\left(\\alpha f\\left(x\\right) + \\beta g\\left(x\\right)\\right) = \\alpha \\frac{df\\left(x\\right)}{dx} + \\beta \\frac{dg\\left(x\\right)}{dx}\n",
    "$$\n",
    " - la règle de Leibniz (ou de dérivation du produit)\n",
    "\n",
    "$$\n",
    "\\frac{df\\left(x\\right) g\\left(x\\right)}{dx} = \\frac{df\\left(x\\right)}{dx} g\\left(x\\right) + f\\left(x\\right) \\frac{dg\\left(x\\right)}{dx}\n",
    "$$\n",
    "\n",
    "Ces deux règles peuvent servir à définir complètement l'opération de dérivée (elles s'appellent dans ce cas la **définition de Leibniz des dérivées**), mais on va plutôt les démontrer ici à partir des définitions de Newton utilisant la notion de limite."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Linéarité de la dérivée\n",
    ":class: dropdown\n",
    "La linéarité se démontre facilement par la définition de Newton : \n",
    "\n",
    "$$\\begin{align}\n",
    "\\frac{d}{dx}\\left(\\alpha f\\left(x\\right)+\\beta g\\left(x\\right)\\right)&=\\lim_{\\epsilon\\rightarrow0}\\left[\\frac{\\alpha f\\left(x+\\epsilon\\right)+\\beta g\\left(x+\\epsilon\\right)-\\alpha f\\left(x\\right)-\\beta g\\left(x\\right)}{\\epsilon}\\right]\\\\&=\\alpha\\lim_{\\epsilon\\rightarrow0}\\left[\\frac{f\\left(x+\\epsilon\\right)-f\\left(x\\right)}{\\epsilon}\\right]+\\beta\\lim_{\\epsilon\\rightarrow0}\\left[\\frac{g\\left(x+\\epsilon\\right)-g\\left(x\\right)}{\\epsilon}\\right]\\\\&=\\alpha\\frac{df\\left(x\\right)}{dx}+\\beta\\frac{dg\\left(x\\right)}{dx}\n",
    "\\end{align}\n",
    "$$\n",
    "```\n",
    "\n",
    "```{admonition} Règle de Leibniz\n",
    ":class: dropdown\n",
    "La règle de Leibniz se démontre en insérant $f\\left(x\\right)g\\left(x+\\epsilon\\right)-f\\left(x\\right)g\\left(x+\\epsilon\\right)$ au numérateur, puis en réordonant\n",
    "\n",
    "$$\\begin{align}\n",
    "\\frac{df\\left(x\\right)g\\left(x\\right)}{dx}\t&=\\lim_{\\epsilon\\rightarrow0}\\frac{f\\left(x+\\epsilon\\right)g\\left(x+\\epsilon\\right)-f\\left(x\\right)g\\left(x\\right)}{\\epsilon} \\\\\n",
    "\t&=\\lim_{\\epsilon\\rightarrow0}\\frac{f\\left(x+\\epsilon\\right)-f\\left(x\\right)}{\\epsilon}g\\left(x+\\epsilon\\right)+f\\left(x\\right)\\lim_{\\epsilon\\rightarrow0}\\frac{g\\left(x+\\epsilon\\right)-g\\left(x\\right)}{\\epsilon} \\\\\n",
    "\t&=\\frac{df\\left(x\\right)}{dx}\\lim_{\\epsilon\\rightarrow0}g\\left(x+\\epsilon\\right)+f\\left(x\\right)\\frac{dg\\left(x\\right)}{dx} \\\\\n",
    "\t&=\\frac{df\\left(x\\right)}{dx}g\\left(x\\right)+f\\left(x\\right)\\frac{dg\\left(x\\right)}{dx} \\\\\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "et en remarquant que $\\lim_{x\\rightarrow x_{0}}a\\left(x\\right)b\\left(x\\right)=\\left[\\lim_{x\\rightarrow x_{0}}a\\left(x\\right)\\right]\\left[\\lim_{x\\rightarrow x_{0}}b\\left(x\\right)\\right]$ si les deux limites sont définies. La dernière ligne s'obtient en remarquant que $\\lim_{\\epsilon\\rightarrow0}g\\left(x+\\epsilon\\right)=g\\left(x\\right)$\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On peut maintenant calculer des dérivées plus complexe, par exemple la règle de dérivée du quotient de deux fonctions dérivables\n",
    "\n",
    "$$\n",
    "\\left(\\frac{f}{g}\\right)^{\\prime}=\\frac{f^{\\prime}g-fg^{\\prime}}{g^{2}}\n",
    "$$\n",
    "\n",
    "en utilisant la règle de Leibniz sur le produit de $f$ par $1/g = g^{-1}$, dont la dérivée est $\\left(g^{-1}\\right)^\\prime = -g^\\prime/g^2$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Dérivée $x^\\alpha$\n",
    ":class: dropdown\n",
    "La démonstration de la dérivée de la puissance de $x$ est aussi intéréssante, puisqu'elle utilise le raisonnement par récurence. On démontre tout d'abord que a propriété est vraie pour $\\alpha=1$ (on le fait pour $\\alpha=2$ puisque c'est à peine plus compliqué, on a $x^{\\prime}=\\lim_{\\epsilon\\rightarrow0}\\left(x+\\epsilon-x\\right)/\\epsilon=\\lim_{\\epsilon\\rightarrow0}1=1$ très facilement)\n",
    "\n",
    "$$\\begin{align}\n",
    "\\left(x^{2}\\right)^{\\prime}&=\\lim_{\\epsilon\\rightarrow0}\\frac{\\left(x+\\epsilon\\right)^{2}-x^{2}}{\\epsilon}\\\\&=\\lim_{\\epsilon\\rightarrow0}\\left[\\frac{x^{2}+2x\\epsilon+\\epsilon^{2}-x^{2}}{\\epsilon}\\right]\\\\&=2x\n",
    "\\end{align}$$\n",
    "\n",
    "puis on suppose la propriété vraie pour $\\alpha$ : $\\left(x^{\\alpha}\\right)^\\prime = \\alpha x^ {\\alpha -1}$, et on regarde ce que cela implique pour $\\left(x^{\\alpha+1}\\right)^\\prime$. On obtient facilement\n",
    "\n",
    "$$\\begin{align}\n",
    "\\left(x^{\\alpha+1}\\right)^{\\prime}&=\\left(x^{\\alpha}x\\right)^{\\prime}\\\\&=\\left(x^{\\alpha}\\right)^{\\prime}x+x^{\\alpha}\\left(x\\right)^{\\prime}\\\\&=\\alpha x^{\\alpha-1}x+x^{\\alpha}\\\\&=\\left(\\alpha+1\\right)x^{\\alpha}\n",
    "\\end{align}$$\n",
    "\n",
    "finnissant la démonstration pour tous les cas $\\alpha$ entiers.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La dérivée de la composition de fonctions est aussi importante (cette formule s'appelle [règle de chaîne](https://en.wikipedia.org/wiki/Chain_rule), ou _chain rule_ en anglais)\n",
    "\n",
    "$$\n",
    "\\frac{df\\left(g\\left(x\\right)\\right)}{dx}=\\frac{df}{dg}\\frac{dg}{dx}\\;;\\;\\left(f\\circ g\\right)^{\\prime}\\left(x\\right)=\\left(f^{\\prime}\\circ g\\right)\\left(x\\right)g^{\\prime}\\left(x\\right)\n",
    "$$\n",
    "\n",
    "où l'on a utilisé la notation $f^{\\prime}\\left(x\\right)$ qui est parfois considérée comme plus naturelle que la notation de Leibniz $f^{\\prime}\\left(x\\right)=\\frac{df\\left(x\\right)}{dx}$ des différentielles $df$ et $dx$. La [preuve](https://en.wikipedia.org/wiki/Chain_rule#Proofs) de ce théorême est un peu subtile (ou directe, suivant le niveau de subtilité du lecteur)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "À partir de ces quelques dérivées et de ces règles de calcul, on peut construire la dérivée de toutes les fonctions. En particulier, on peut calculer que \n",
    "\n",
    " - $\\left(e^{u\\left(x\\right)}\\right)^{\\prime} = u^\\prime\\left(x\\right)e^{u\\left(x\\right)}$ par application directe de la formule de composition sur $\\left(e^x\\right)^\\prime = e^x$\n",
    " - $\\left(\\left[u\\left(x\\right)\\right]^{\\alpha}\\right)^{\\prime}=\\alpha u^{\\prime}\\left(x\\right)\\left[u\\left(x\\right)\\right]^{\\alpha-1}$ à partir de la dérivée de $x^\\alpha$, \n",
    " - $\\left(\\ln x\\right)^\\prime = \\frac{1}{x}$ à partir de la dérivée de la définition $e^{\\ln x}=x$ que l'ond érive des deux côtés\n",
    " - $\\left(\\arcsin x\\right)^\\prime = \\frac{1}{\\sqrt{1-x^2}}$ et de même pour les [autres fonctions trigonométriques inverses](https://en.wikipedia.org/wiki/Inverse_trigonometric_functions#Derivatives_of_inverse_trigonometric_functions)\n",
    "\n",
    "depuis leur fonction inverse.\n",
    "\n",
    "```{admonition} Dérivée du logarithme\n",
    ":class: dropdown\n",
    "La dérivée du logarithme s'obtient en calculant la dérivée de $e^{\\ln x} = x$. On obtient \n",
    "\n",
    "$$\\begin{align}\n",
    "\\frac{de^{\\ln x}}{dx}=1\\Rightarrow&\\left.\\frac{de^{x}}{dx}\\right|_{x=\\ln x}\\frac{d\\ln x}{dx}=1\\\\\\Rightarrow&x\\frac{d\\ln x}{dx}=1\\\\\\Rightarrow&\\frac{d\\ln x}{dx}=\\frac{1}{x}\n",
    "\\end{align}$$\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dérivée et analyse de fonction\n",
    "\n",
    "La dérivée d'une fonction $f^{\\prime}\\left(x_0\\right)$ représente la tangente à la fonction $f\\left(x\\right)$ au point $x=x_0$ : c'est la droite affine passant au mieux au point $x_0$. La dérivée seconde $f^{\\prime\\prime}\\left(x_0\\right)$ représente la courbure de la fonction au point $x_0$ : c'est la parabole épousant au mieux la forme de la courbe tracée depuis $f\\left(x\\right)$ au point $x_0$.\n",
    "\n",
    "On peut donc approcher n'importe quelle fonction autour du point $x=x_0$ par des polynômes de la forme $f\\left(x\\approx x_0\\right)\\approx f\\left(x_0\\right) + \\left(x-x_0\\right)f^{\\prime}\\left(x_0\\right) + \\left(x-x_0\\right)^2 f^{\\prime\\prime}\\left(x_0\\right)/2 + \\cdots$, ce genre de développement s'appelant une [série de Taylor](series_developpements_limites).\n",
    "\n",
    "Un point pour lequel la dérivée s'annule est toujours un point spécial pour la fonction.\n",
    "\n",
    "Si la dérivée première s'annule, on parle de [point stationnaire](https://fr.wikipedia.org/wiki/Point_stationnaire) de la fonction, cela correspond à un maximum ou à un minimum local. Pour savoir quel extremum (ou optimum) est représenté par cette annulation de dérivée, on calcule la dérivée seconde, qui sera positive si c'est un minimum et négative si c'est un maximum."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dérivée symbolique\n",
    "\n",
    "Certaines librairies (ou logiciels) permettent de calculer des dérivées de façon symbolique. C'est le cas notamment de [sympy](https://www.sympy.org/en/index.html). "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pouvoir calculer la dérivée symbolique d'une fonction est de prime importance pour certains problème d'optimisation, comme les problèmes d'[apprentissage de réseaux de neuronnes](https://en.wikipedia.org/wiki/Backpropagation), pour lesquels ont été inventé un nouveau paradigme de programmation dit [programmation différentielle](https://en.wikipedia.org/wiki/Differentiable_programming)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dérivée numérique\n",
    "\n",
    "Dériver une fonction n'est pas une procédure numérique. Il nous faut des méthodes d'approximations pour les calculer une fois connus un ensemble de points représentatifs de la fonction  elle-même. Techniquement, calculer les dérivées de façon numérique est un problème numérique, étudié dans [la section du numérique de ces notes](../numerique/derivation_numerique)."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
