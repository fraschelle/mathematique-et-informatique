#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some sequences (or map). One calculates only the next element of the sequences, by calling the `next` method.

By default initial values are random, and parameters are not instanciated with default values.

One can also use the `restart()` method to start again the sequence.
"""

from random import random
from math import sin, pi


class DuffingMap():
    """https://en.wikipedia.org/wiki/Duffing_map"""
    def __init__(self, 
                 a: float, 
                 b: float, 
                 x0: float = random(), 
                 y0: float = random()):
        self.x0 = float(x0)
        self.y0 = float(y0)
        self.a = float(a)
        self.b = float(b)
        self.x = self.x0
        self.y = self.y0
    def restart(self):
        self.x = self.x0
        self.y = self.y0
    def __next__(self):
        self.x, self.y = self.y, self.a*self.y - self.b * self.x - self.y**3
        return self.x, self.y

class GingerbreadMap():
    """https://en.wikipedia.org/wiki/Gingerbreadman_map"""
    def __init__(self, 
                 x0: float = 10*random(), 
                 y0: float = 10*random()):
        self.x0 = float(x0)
        self.y0 = float(y0)
        self.x = self.x0
        self.y = self.y0
    def restart(self):
        self.x = self.x0
        self.y = self.y0
    def __next__(self):
        self.x, self.y = 1 - self.y + abs(self.x), self.x
        return self.x, self.y


class HénonMap():
    """https://en.wikipedia.org/wiki/H%C3%A9non_map"""
    def __init__(self, 
                 a: float, 
                 b: float, 
                 x0: float = random(), 
                 y0: float = random()):
        self.x0 = float(x0)
        self.y0 = float(y0)
        self.a = float(a)
        self.b = float(b)
        self.x = self.x0
        self.y = self.y0
    def restart(self):
        self.x = self.x0
        self.y = self.y0
    def __next__(self):
        self.x, self.y = 1 - self.a * pow(self.x, 2) + self.y, self.b * self.x
        return self.x, self.y


class LogisticMap():
    """https://en.wikipedia.org/wiki/Logistic_map"""
    def __init__(self, 
                 mu: float, 
                 x0: float = random()):
        self.x0 = float(x0)
        if self.x0 > 1 or self.x0 < 0:
            raise ValueError("x0 must be in [0,1]")
        self.mu = float(mu)
        self.value = self.x0
    def restart(self):
        self.value = self.x0
    def __next__(self):
        self.value = self.mu * self.value * (1-self.value)
        return self.value


class StandardMap():
    """https://en.wikipedia.org/wiki/Standard_map"""
    def __init__(self, 
                 K: float, 
                 p0: float = 2*pi*random(), 
                 t0: float = 2*pi*random()):
        self.p0 = float(p0) % (2*pi)
        self.t0 = float(t0) % (2*pi)
        self.K = float(K)
        self.p = self.p0
        self.t = self.t0
    def restart(self):
        self.p = self.p0
        self.t = self.t0
    def __next__(self):
        self.p = (self.p + self.K*sin(self.t % (2*pi))) % (2*pi)
        self.t = (self.t + self.p) % (2*pi)
        return self.p, self.t
    