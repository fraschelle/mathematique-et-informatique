#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tests of the Polynomial class.

Run this test as
```bash
python -m unittest -v
```
from a console in the parent folder (verbosity -v is an option).
"""

import unittest as ut
from fractions import Fraction
from math import factorial

from polynomial import Polynomial, divide_series


class TestPolynomial(ut.TestCase):

    def test_exist(self):
        """Existance of Polynomial class and its attributes."""
        p = Polynomial({0:2, 1:4, 3:7, 5:-1})
        self.assertIsInstance(p, dict)
        self.assertTrue(hasattr(p, 'degree'))
        self.assertEqual(p.degree, 5)
        return None
    
    def test_addition(self):
        """Addition of two Polynomial."""
        p1 = Polynomial({0:2, 1:4, 3:7, 5:-1})
        p2 = Polynomial({0:1, 1:2, 2: 3,3:1, 5:1})
        result = Polynomial({0:3, 1:6, 2:3, 3:8})
        self.assertEqual(p1+p2, result)
        return None
    
    def test_substraction(self):
        """Substraction of two Polynomial."""
        p1 = Polynomial({0:2, 1:4, 3:7, 5:-1})
        p2 = Polynomial({0:1, 1:2, 2: 3,3:1, 5:1})
        result = Polynomial({0:1, 1:2, 2:-3, 3:6, 5:-2})
        self.assertEqual(p1-p2, result)
        self.assertEqual(p2-p1, -result)
        return None

    def test_multiplication(self):
        """Multiplication of two Polynomial."""
        p1 = Polynomial({0:2, 1:4, 3:7, 5:-1})
        p2 = Polynomial({0:1, 1:2, 2: 3,3:1, 5:1})
        result = Polynomial({0:2, 1:8, 2:14, 3:21, 4:18, 5:22, 6:9,
                             7:-3, 8:6, 10:-1})
        self.assertEqual(p1*p2, result)
        constant = Polynomial({0:7})
        result = Polynomial({e:7*c for e,c in p1.items()})
        self.assertEqual(p1*constant, result)
        fraction = Polynomial({0:Fraction(3,7)})
        result = Polynomial({e:c*Fraction(3,7) for e,c in p1.items()})
        self.assertEqual(p1*fraction, result)
        return None

    def test_division(self):
        """Euclidian division of two Polynomial."""
        A = Polynomial({0:1, 2:-1, 3:1})
        B = Polynomial({2:1})
        Q, R = divmod(A, B)
        Qresult = Polynomial({0:-1, 1:1})
        Rresult = Polynomial({0:1})
        self.assertEqual(Q, Qresult)
        self.assertEqual(R, Rresult)
        self.assertEqual(B * Q + R, A)
        self.assertEqual(A//B, Qresult)
        self.assertEqual(A%B, Rresult)
        # from https://fr.wikipedia.org/wiki/Division_d%27un_polyn%C3%B4me#Exemple_et_algorithme
        A = Polynomial({5:1, 4:-1, 3:-1, 2:3, 1:-2})
        B = Polynomial({2:1, 1:-1, 0:1})
        Q, R = divmod(A, B)
        Qresult = Polynomial({0:1, 1:-2, 3:1})
        Rresult = Polynomial({0:-1, 1:1})
        self.assertEqual(Q, Qresult)
        self.assertEqual(R, Rresult)
        self.assertEqual(B * Q + R, A)
        self.assertEqual(A//B, Qresult)
        self.assertEqual(A%B, Rresult)        
        return None
    
    def test_tanh(self):
        """Construction of tanh from Taylor series of exp."""
        exp = Polynomial({n: Fraction(1, factorial(n))
                          for n in range(10)})
        exp_ = Polynomial({n: Fraction(pow(-1, n), factorial(n))
                           for n in range(10)})
        cosh = (exp + exp_)/2
        # from https://en.wikipedia.org/wiki/Taylor_series#Hyperbolic_functions
        cosh_result = Polynomial({2*n: Fraction(1, factorial(2*n))
                                  for n in range(5)})
        self.assertEqual(cosh, cosh_result)
        sinh = (exp - exp_)/2
        # from https://en.wikipedia.org/wiki/Taylor_series#Hyperbolic_functions
        sinh_result = Polynomial({2*n+1: Fraction(1, factorial(2*n+1))
                                  for n in range(5)})
        self.assertEqual(sinh, sinh_result)
        # multiply both series by 1/coeff of the degree of sinh
        constant = Polynomial({0:Fraction(1,sinh[sinh.degree])})
        tanh, rest = divide_series(sinh, cosh)
        # from https://en.wikipedia.org/wiki/Taylor_series#Hyperbolic_functions
        tanh_result = Polynomial({1:1, 3:Fraction(-1,3), 
                                  5:Fraction(2,15), 7:Fraction(-17,315)})
        self.assertEqual(tanh, tanh_result)
        self.assertEqual(tanh * cosh + rest, sinh)
        return None
        


