#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Polynomial class represents a polynomial function with Fraction coefficients.
"""

from fractions import Fraction

def divide_series(numer, denom):
    """Division of two polynomials called numer N and denom D, in the case of series approximation.
    
    Return two Polynomial, the quotient Q and the rest N.
    
    The rest of the divison has degree greater than numer or denom Polynomial, but still verifies that N = Q * D + R (multiplication in the sense of Polynomial).
    """
    Q, R = Polynomial({}), Polynomial({e:c for e,c in numer.items()})
    deg_rest, deg_denom = min(R.keys()), min(denom.keys())
    while deg_rest < denom.degree:
        coeff = Fraction(R[deg_rest], denom[deg_denom])
        Qmonomial = Polynomial({deg_rest-deg_denom: coeff})
        R -= Qmonomial * denom
        Q += Qmonomial
        deg_rest = min(R.keys())
    return Q, R

class Polynomial(dict):
    
    """
    A Polynomial represents a mathematical polynomial.
    
    SubClass of dict, a Polynomial is instanciated via a dictionnary of type {exponent: coefficient}. There is no check of the consistency of the coefficients for simplicity.
    
    Example
    -------
    
    > p1 = Polynomial({0: 2, 1: 4, 3: 7, 5: -1})
    > p2 = Polynomial({0: 1, 1: 2, 2: 3, 3: 1, 5: 1})
    
    One can then do addition, substraction, multiplication and/or division of polynomials.
    """
    
    def __check_polynomial(self, polynomial, algebraic_operation):
        """Raise Exceptions in case `polynomial` is not a valid Polynomial instance."""
        if not isinstance(polynomial, self.__class__):
            mess = f"unsupported operand type(s) for {algebraic_operation}: "
            mess += "'{}'".format(self.__class__.__name__)
            mess += " and '{}'".format(polynomial.__class__.__name__)
            raise TypeError(mess)
        if min(polynomial.keys()) < 0:
            mess = "{}".format(self.__class__.__name__)
            mess += " can not represent negative power(s) of monomial"
            raise ValueError(mess)
        return True
    
    def __call__(self, x):
        """Evaluate the Polynomial at point x."""
        return sum(c*pow(x,e) for e,c in self.items())
    
    @property
    def degree(self):
        """The degree of a Polynomial is its maximal power."""
        return max(self.keys())
    
    def __add__(self, polynomial):
        """Addition of two Polynomials"""
        self.__check_polynomial(polynomial, '+')
        result = Polynomial({e:c for e,c in self.items()})
        for exponent, coefficient in polynomial.items():
            result[exponent] = result.get(exponent, 0) + coefficient
        result = Polynomial({e:c for e,c in result.items() if c != 0})
        return result

    def __sub__(self, polynomial):
        """Difference of two Polynomials."""
        self.__check_polynomial(polynomial, '-')
        result = Polynomial({e:c for e,c in self.items()})
        for exponent, coefficient in polynomial.items():
            result[exponent] = result.get(exponent, 0) - coefficient
        result = Polynomial({e:c for e,c in result.items() if c != 0})
        return result
    
    def __neg__(self):
        """Negative value of a Polynomial, for - Polynomial handling."""
        return self.__class__({e:-c for e,c in self.items()})

    def __mul__(self, polynomial):
        """Multiplication of two Polynomials."""
        self.__check_polynomial(polynomial, '*')
        result = Polynomial({})
        for exp1, c1 in self.items():
            for exp2, c2 in polynomial.items():
                result[exp1 + exp2] = result.get(exp1 + exp2, 0) + c1 * c2
        result = Polynomial({e:c for e,c in result.items() if c != 0})
        return result
    
    def __truediv__(self, polynomial):
        """The true division exists for Fraction, but not for Polynomial"""
        if isinstance(polynomial, int):
            return self.__class__({e:Fraction(c, polynomial)
                                   for e,c in self.items()})
        else:
            mess = "usual division does not exist for "
            mess += "{}".format(self.__class__.__name__)
            mess += "\tTry // or % or divmod"
            raise AttributeError(mess)
        return None
    
    def __floordiv__(self, polynomial):
        """Quotient of the division of Polynomial by polynomial."""
        self.__check_polynomial(polynomial, '//')
        return divmod(self, polynomial)[0]
    
    def __mod__(self, polynomial):
        """Rest of the division of Polynomial by polynomial."""
        self.__check_polynomial(polynomial, '%')
        return divmod(self, polynomial)[1]
    
    def __divmod__(self, polynomial):
        """Euclidian division of two Polynomials. Return a tuple of two Polynomial objects."""
        self.__check_polynomial(polynomial, 'divmod')
        Q, R = Polynomial({}), Polynomial({e:c for e,c in self.items()})
        degree = R.degree - polynomial.degree
        while degree >= 0:
            coeff = R[R.degree] // polynomial[polynomial.degree]
            Qmonomial = Polynomial({degree: coeff})
            R -= Qmonomial * polynomial
            Q += Qmonomial
            degree = R.degree - polynomial.degree
        return Q, R
        
    def __repr__(self):
        return self.__class__.__name__ + "(" + str(self) + ")"

    def __str__(self):
        result = [""]+[f"{c}x^{e}" for e,c in sorted(self.items()) if c]+[""]
        result = "+".join(reversed(result))
        result = result.replace("x^0","")
        result = result.replace("+0","")
        result = result.replace("+1x","+x")
        result = result.replace("-1x","-x")
        result = result.replace("x^1+","x+")
        result = result.replace("+-","-")
        result = result.strip("+")
        result = result.replace("+"," + ")
        result = result[:1]+result[1:].replace("-"," - ")
        return result.strip()
