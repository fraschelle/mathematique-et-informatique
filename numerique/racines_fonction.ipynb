{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6cbe7b5a-089d-4001-a268-f3f4f1171f05",
   "metadata": {},
   "source": [
    "# Racines d'une équation\n",
    "\n",
    "Trouver les [racines d'une fonction](https://fr.wikipedia.org/wiki/Z%C3%A9ro_d%27une_fonction) est un problème générique qui intervient dans de nombreux problèmes informatiques. Par exemple, un problème d'optimisation consiste à trouver le minimum (maximum) d'une fonction de coût (de gain), ce qui se calcule comme les zéros de la dérivée de cette fonction de coût ou de gain.\n",
    "\n",
    "Le problème général consiste à trouver l'ensemble des points $x_{i}$ tels que $f\\left(x_{i}\\right) = 0$ pour chacun de points et pour une fonction $f$ donnée. Ce sont ces points qui sont appelé **racines** de l'équation $f\\left(x_{i}\\right) = 0$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69726a53-f70e-40b1-a900-7acc0758d782",
   "metadata": {},
   "source": [
    "## Calcul explicite : fonction polynomiale\n",
    "\n",
    "Dans le cas d'une fonction polynomiale, on peut trouver une solution exacte et explicite des racines. Il y a en général une solution par degrès maximum du polynôme. \n",
    "\n",
    "Ainsi, pour une équation linéaire $ax+b = 0$, on obbeitn l'ensemble des racines sous la forme du singulet $x_{0} = -b/a$, qui n'existe que lorsque $a \\neq 0$.\n",
    "\n",
    "Une [équation quadratique](https://fr.wikipedia.org/wiki/%C3%89quation_du_second_degr%C3%A9) (i.e. pour un polynôme d'ordre deux) $ax^2 + bx +c = 0$ admet deux solutions \n",
    "\n",
    "$$x_{\\pm} = -\\frac{b}{2a} \\pm \\frac{\\sqrt{b^{2} - 4ac}}{2a}$$\n",
    "\n",
    "et on voit alors que le cas $a=0$ est pathologique, que les deux solutions n'en font qu'une lorsque $b^{2}-4ac = 0$ et qu'elles sont réelles (imaginaires ou inexistantes) lorsque $b^{2}-4ac > 0$ ($b^{2}-4ac < 0$, respectivement). \n",
    "\n",
    "On peut continuer à essayer de trouver des solutions pour les [équations cubiques](https://fr.wikipedia.org/wiki/%C3%89quation_cubique) ou de plus haut degrès, mais ces solutions ne sont pas générales. Dans les cas de plus haut degrès, ou lorsque la fonction n'est pas polynomiale, on a recours à des méthodes de résolutions numériques de recherche des racines d'une équation.  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8071151b-24a5-49b4-a3d4-20701418ba2a",
   "metadata": {},
   "source": [
    "## Méthode de la dichotomie\n",
    "\n",
    "La [méthode de recherche de racine par dichotomie]() est sans doute la méthode la plus simple. Elle consiste à remarquer que la fonction $f\\left(x\\right)$ change de signe autour d'une de ses racines. On essaye donc de s'approcher au plus prés de la racine par découpage successif de l'intervale de départ en deux, et à recommencer jusqu'à obtenir la précision numérique voulue.\n",
    "\n",
    "Si on arrive à isoler un intervale $\\left[a,b\\right]$ tel que $f\\left(a\\right)f\\left(b\\right) < 0$, alors on peut calculer la valeur  $f\\left(x_{m}\\right)$ de la fonction $f$ au milieu de l'intervale $\\left[a,b\\right]$, c'est-à-dire au point $x_{m} = \\frac{a+b}{2}$ et il n'y aura que deux cas possibles \n",
    " \n",
    "  - soit $f\\left(a\\right)f\\left(x_{m}\\right) < 0$, auquel cas la racine se trouve entre les points $a$ et $x_m$\n",
    "  - soit $f\\left(a\\right)f\\left(x_{m}\\right) > 0$, auquel cas la racine se trouve entre les points $x_{m}$ et $b$ (on pourrait d'ailleurs vérifier que $f\\left(b\\right)f\\left(x_{m}\\right) < 0$ dans ce cas).\n",
    "  \n",
    "et on peut recommencer l'analyse en déplaçant la borne supérieure ($b$) ou inférieure ($a$) vers le point médian à l'étape supérieure.\n",
    "\n",
    "L'erreur absolue $\\varepsilon$ entre la vraie solution et la solution approchée, au bout de l'étape $N$, est alors de au plus égale à $\\varepsilon=\\frac{b-a}{2^N}$, et (inversant cette relation), on obtient qu'il faut $N > \\log_{2}\\frac{b-a}{\\varepsilon}$ étapes pour obtenir la précision $\\varepsilon$ sur la valeur vraie."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b681375-7e07-466f-bbdf-55ab8503d99a",
   "metadata": {},
   "source": [
    "## Méthode de Newton, ou descente de gradient\n",
    "\n",
    "La méthode de Newton consiste à supposer que la fonction $f\\left(x\\right)$, autour d'une de ses racines $x$, aura une forme approximative \n",
    "\n",
    "$$f\\left(x\\right) \\approx f\\left(x_{0}\\right) + f^{\\prime}\\left(x_{0}\\right)\\left(x-x_{0}\\right)$$\n",
    "\n",
    "(par calcul du développement limité tronqué au premier ordre) donc on peut espérer se rapprocher de la valeur $x$ telle que $f\\left(x\\right) = 0$ en faisant les itérations \n",
    "\n",
    "$$x_{k+1} = x_{k} - \\frac{f\\left(x_{k}\\right)}{f^{\\prime}\\left(x_{k}\\right)}$$\n",
    "\n",
    "qui vérifie l'équation $f\\left(x_{k}\\right)+f^{\\prime}\\left(x_{k}\\right)\\left(x-x_{k}\\right)=0$.\n",
    "\n",
    "L'enjeu de cette méthode est alors de se positionner à proximité d'une racine de l'équation, pour ne pas que la méthode de Newton aille explorer une large région du domaine de la fonction $f$.\n",
    "\n",
    "La fonction $f^{\\prime}$ est bien évidemment la **dérivée** de la fonction $f$. Il faut donc connaître cette fonction pour appliquer la méthode de Newton. Lorsqu'il y a plusieurs variables, la dérivée se généralise en la notion de *gradient*, et l'on parle parfois de *descente de gradient* pour la méthode d'optimisation qui utilise la méthode de Newton."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fa134f40-cf3f-4984-b6dd-d2160b8bc35d",
   "metadata": {},
   "source": [
    "## Méthode de la sécante\n",
    "\n",
    "Si l'on ne connais pas la dérivée de $f$, on ne peut utiliser la méthode de Newton. Mais on peut approximer cette dérivée à proximité d'une racine à l'aide de l'approximation numérique\n",
    "\n",
    "$$f^{\\prime}\\left(x\\in\\left[a,b\\right]\\right) \\approx \\frac{f\\left(b\\right)-f\\left(a\\right)}{b-a}$$\n",
    "\n",
    "La méthode de la sécante a alors pour formule d'itération \n",
    "\n",
    "$$x_{n+1} = x_{n} - \\frac{x_{n}-x_{n-1}}{f\\left(x_{n}\\right)-f\\left(x_{n-1}\\right)} f\\left(x_{n}\\right)$$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
