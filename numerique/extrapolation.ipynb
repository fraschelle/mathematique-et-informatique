{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a72a32f6",
   "metadata": {},
   "source": [
    "# Régression et extrapolation\n",
    "\n",
    "Contrairement au problème d'interpolation qui consiste à passer par un ensemble de points donnés, les problèmes de régression ou d'extrapolation ne supposent pas de passer par l'ensemble de ces points. Il s'agit plutôt de trouver une _meilleure_ représentation de l'ensemble de ces points, de façon plus économe (on parle alors de compression) ou plus symmétrique (on parle alors de lissage)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "ab169d46",
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [],
   "source": [
    "from typing import List, Tuple\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "508d6282",
   "metadata": {},
   "source": [
    "## Régression"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8810fe3a",
   "metadata": {},
   "source": [
    "## Courbes de Bézier\n",
    "\n",
    "Les [courbes de Bézier](https://fr.wikipedia.org/wiki/Courbe_de_B%C3%A9zier) consiste à encadrer une courbe par un petit nombre de points, et de faire en sorte que cette courbe soit unique et lisse. Elles servent abondament dans les contextes du dessin industriel (où elles ont été inventées) et de la représentation des graphiques par ordinateur (jeux vidéo, police de caractères, logiciel de retouche d'image, ... ) puisqu'elles sont à la base de ce que l'on appelle le [dessin vectoriel](https://fr.wikipedia.org/wiki/Image_vectorielle)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d8d1738",
   "metadata": {},
   "source": [
    "La formule générale d'écriture d'une courbe de Bézier à deux dimensions (dans le plan $\\left(x,y\\right)$ donc) s'écrit\n",
    "\n",
    "$$\n",
    "\\mathbf{B}\\left(t\\right) = \\sum_{i=0}^n {n\\choose i}(1 - t)^{n - i}t^i\\mathbf{P}_i\n",
    "$$\n",
    "\n",
    "où \n",
    "\n",
    " - le paramètre $t$ varie sur l'intervale $\\left[0,1\\right]$ uniquement,\n",
    " - les coefficients ${n \\choose i} = \\frac{n!}{i!(n - i)!}$ sont les [coefficients binomiaux](https://fr.wikipedia.org/wiki/Coefficient_binomial)\n",
    " - les $\\mathbf{P}_i$ sont les points dit **de contrôle** de la courbe de Bézier, il s'écrivent $\\mathbf{P}_i = \\left(x_{i}, y_{i}\\right)$. Il y a $n$ points de contrôle pour une courbe d'ordre $n$. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3f8ebde",
   "metadata": {},
   "source": [
    "L'écriture ci-dessus est vectorielle, puisqu'elle représente un parcours en deux dimensions, c'est-à-dire une courbe paramétrique, dont le paramètre est $t$. On peut réécrire cette courbe sous la forme vectorielle explicite $\\mathbf{B}\\left(t\\right)=\\left(B_{x}\\left(t\\right), B_{y}\\left(t\\right)\\right)$, et on aura donc \n",
    "\n",
    "\\begin{align}\n",
    "B_{x}\\left(t\\right) &= \\sum_{i=0}^n {n\\choose i}(1 - t)^{n - i}t^i x_i \\\\\n",
    "B_{y}\\left(t\\right) &= \\sum_{i=0}^n {n\\choose i}(1 - t)^{n - i}t^i y_i\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddaa157a",
   "metadata": {},
   "source": [
    "```{admonition} Polynôme de Bernstein\n",
    ":class: hint\n",
    "\n",
    "La courbe de Bézier peut s'écrire encore\n",
    "\n",
    "$$\n",
    "\\mathbf{B}\\left(t\\right) = \\sum_{i=0}^n b_{n,i}\\left(t\\right)\\mathbf{P}_i \\;,\\; b_{n,i} = {n\\choose i}(1 - t)^{n - i}t^i\n",
    "$$\n",
    "\n",
    "où les $b_{n,i}$ sont les [polynômes de Bernstein](https://fr.wikipedia.org/wiki/Polyn%C3%B4me_de_Bernstein) de degrés $n$.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18c18d2b",
   "metadata": {},
   "source": [
    "Parmis les propriétés importantes des courbes de Bézier, remarquons que\n",
    "\n",
    " - en $t=0$, on a $\\mathbf{B}\\left(t=0\\right)=\\mathbf{P}_{0}$, la courbe de Bézier passe par le premier point\n",
    " - en $t=1$, on a $\\mathbf{B}\\left(t=1\\right)=\\mathbf{P}_{n}$, la courbe de Bézier passe par le dernier point\n",
    " \n",
    "Et pendant son tracé, de $t=0$ à $t=1$, la courbe paramétrique de Bézier ne va plus en général passer par un autre point $\\mathbf{P}_{i}$, sauf cas particulier."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c9883e3",
   "metadata": {},
   "source": [
    "### Algorithme deCasteljau de calcul des courbes de Bézier\n",
    "\n",
    "Du fait de la condition de récurence\n",
    "\n",
    "$$\n",
    "{n \\choose k} + {n \\choose k+1} = {n+1 \\choose k+1}\n",
    "$$\n",
    "\n",
    "des coefficients binomiaux, on peut écrire les courbes de Bézier à l'aide de l'[algorithme de deCasteljau](https://fr.wikipedia.org/wiki/Algorithme_de_Casteljau), qui permet de résoudre les problèmes d'instabilité numérique liés à l'évaluation des sommes de polynômes de Bernstein.\n",
    "\n",
    "Cet algorithme utilise la représentation \n",
    "\n",
    "$$\n",
    "\\mathbf{B}\\left(t\\right) = \\sum_{i=0}^n b_{n,i}\\left(t\\right)\\mathbf{P}_i \\;,\\; b_{n,i} = {n\\choose i}(1 - t)^{n - i}t^i\n",
    "$$\n",
    "\n",
    "pour lequel on remplit un tableau $\\beta$ correspondant aux tuples de points de contrôle, et on modifie les valeurs de ce tableau autant de fois qu'il y a de points de contrôle. Ainsi, le tableau est instancié sous la forme\n",
    "\n",
    "$$\\beta^{(0)} = \\left[\\left(x_{0}, y_{0}\\right), \\left(x_{1}, y_{1}\\right), \\ldots, \\left(x_{i}, y_{i}\\right), \\ldots \\right],\\ \\ i=0,\\ldots,n$$\n",
    "\n",
    "puis à chaque étape on réduit les éléments modifiés sous la forme\n",
    "\n",
    "$$\\beta_i^{(j)} = \\beta_i^{(j-1)} (1-t_0) + \\beta_{i+1}^{(j-1)} t_0,\\ \\ i = 0,\\ldots,n-j,\\ \\ j= 1,\\ldots,n$$\n",
    "\n",
    "et à la fin on aura la valeur de la courbe de Bézier au point $t_{0}$ à la place du premier coefficient du tableau, après $n$ itérations\n",
    "\n",
    "$$\\mathbf{B}(t_0) = \\beta_0^{(n)}$$\n",
    "\n",
    "les autres coefficients ne servant plus à rien."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "afba7631",
   "metadata": {},
   "outputs": [],
   "source": [
    "def deCasteljau(t: float, coefs: List[Tuple[float, float]]) -> Tuple[float, float]:\n",
    "    \"\"\"Evaluate the Bézier curve at point t, using deCasteljau's algorithm.\"\"\"\n",
    "    # values in this list are overridden\n",
    "    beta = [(x,y) for x, y in coefs] \n",
    "    for j in range(1, len(coefs)):\n",
    "        for k in range(len(coefs) - j):\n",
    "            beta[k] = (beta[k][0] * (1 - t) + beta[k + 1][0] * t, \n",
    "                       beta[k][1] * (1 - t) + beta[k + 1][1] * t)\n",
    "    return beta[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f95836ed",
   "metadata": {},
   "source": [
    "### Représentation en classe des courbes de Bézier\n",
    "\n",
    "On représente les courbes de Bézier paramétrées par un ensemble de points sous la forme usuelle du `__init__` qui prend en charge les paramètres de la courbes (ici les points `(x,y)` donnés sous forme de tuples) et la méthode `__call__` qui prend en charge l'évaluation (au point `0 <= t <= 1`) de la courbe de Bézier. Cette dernière méthode renvoi le tuple `(x,y)` représentant la position dans le plan de la courbe de Bézier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "3157fe18",
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [],
   "source": [
    "def C(n: int, k: int):\n",
    "    \"\"\"Calculate the C(n,k) binomial combination coefficient.\"\"\"\n",
    "    if n < 2*k: \n",
    "        if n < k:\n",
    "            raise ValueError(\"n must be larger than k\")\n",
    "        return C(n, n-k)\n",
    "    res, denom = 1, 1\n",
    "    for m in range(n-k+1, n+1):\n",
    "        res = (res * m) // denom\n",
    "        denom += 1\n",
    "    return res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "6606b713",
   "metadata": {},
   "outputs": [],
   "source": [
    "class BézierCurve():\n",
    "    \"\"\"Given a set of control points, one can plot a curve developped on the Bernstein polynomials, \n",
    "    called a Bézier curve. The control points (x,y) are given in the form of 2D-tuple. \n",
    "    They represent point in the plane, and construct a curve in 2D.\n",
    "    \"\"\"\n",
    "    \n",
    "    def __init__(self, points):\n",
    "        if any(len(point) != 2 for point in points):\n",
    "            raise ValueError(\"all Ps must be a tuple of same length\")\n",
    "        self.n = len(points) - 1\n",
    "        self.points = list(points)\n",
    "        return None\n",
    "    \n",
    "    def _check_t(self, t):\n",
    "        if not 0 <= t <= 1:\n",
    "            raise ValueError(\"t must be in [0,1] interval\")\n",
    "        return None\n",
    "    \n",
    "    def __call__(self, t: float) -> Tuple[float, float]:\n",
    "        \"\"\"Evaluate the Bézier curve at point t using brute force algorithm.\"\"\"\n",
    "        self._check_t(t)\n",
    "        n = self.n\n",
    "        x = sum(C(n,i)*(1-t)**(n-i)*t**i*self.points[i][0] for i in range(n+1))\n",
    "        y = sum(C(n,i)*(1-t)**(n-i)*t**i*self.points[i][1] for i in range(n+1))\n",
    "        return x, y"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "173d8c7c",
   "metadata": {},
   "source": [
    "```{admonition} Algorithme de deCasteljau\n",
    ":class: example\n",
    "On peut préférer utiliser l'algorithme de deCasteljau pour évaluer la valeur de la courbe de Bézier au point correspondant plutôt qu'un algorithme de brute force comme ci-dessus.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8cc079d9",
   "metadata": {},
   "source": [
    "On trace/modifie alors les courbes de Béziers en déplaçant les points de contrôles."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "a61a6a2e",
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAiMAAAGdCAYAAADAAnMpAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjYuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8o6BhiAAAACXBIWXMAAA9hAAAPYQGoP6dpAABBn0lEQVR4nO3de1iUZd4H8O9wGs6DIEcZBE8gIHjAA57KPGto5XZaLa1tN1pTk9xM2zdr35LcajO3FrNVy9yyA1q45qkUUPOEAiKCoqKcQUQZDsIwM8/7xyhvJCggcM/h+7muubrmmWec30wwz5fnue/fLZMkSQIRERGRIBaiCyAiIiLzxjBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCSUlegCWkOn06GoqAhOTk6QyWSiyyEiIqJWkCQJVVVV8PHxgYVFy+c/jCKMFBUVQalUii6DiIiI2iE/Px++vr4tPm4UYcTJyQmA/s04OzsLroaIiIhaQ6VSQalUNh7HW2IUYeTWpRlnZ2eGESIiIiNztyEWHMBKREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCQUwwgREREJZRRNz4iIiKjjaXUSjuVWoKyqDh5OthgW4ApLi65fA+6ezozExsZCJpPhpZdeuuN+SUlJGDJkCGxtbdGrVy+sXbv2Xl6WiIiI7tGu08UYvWofnvz0CBZtScOTnx7B6FX7sOt0cZfX0u4wcvz4caxbtw5hYWF33C83NxfTpk3DmDFjkJqaiuXLl2PhwoWIj49v70sTERHRPdh1uhgvbD6J4sq6JttLKuvwwuaTXR5I2hVGqqurMXv2bHz66afo1q3bHfddu3Yt/Pz8sHr1avTv3x/PPfccnn32Wbz33nvtKpiIiIjaT6uT8Ob2M5CaeezWtje3n4FW19wenaNdYWT+/PmYPn06JkyYcNd9Dx8+jEmTJjXZNnnyZKSkpKChoaHZ59TX10OlUjW5ERER0b07lltx2xmRX5MAFFfW4VhuRZfV1OYwsmXLFpw8eRKxsbGt2r+kpASenp5Ntnl6ekKj0aC8vLzZ58TGxkKhUDTelEplW8skIiKiZpRVtRxE2rNfR2hTGMnPz8eiRYuwefNm2Nratvp5v106WJKkZrffsmzZMlRWVjbe8vPz21ImERERtcDDqXXH79bu1xHaNLX3xIkTKCsrw5AhQxq3abVaJCcn46OPPkJ9fT0sLS2bPMfLywslJSVNtpWVlcHKygpubm7Nvo5cLodcLm9LaURERNQKwwJc4a2wRUllXbPjRmQAvBT6ab5dpU1nRsaPH4+MjAykpaU13iIiIjB79mykpaXdFkQAIDIyEnv37m2ybc+ePYiIiIC1tfW9VU9ERERtYmkhw4qoYAD64PFrt+6viAru0n4jbQojTk5OCA0NbXJzcHCAm5sbQkNDAegvsTz99NONz4mOjsbly5cRExODrKwsbNiwAevXr8eSJUs69p0QERFRq0wJ9UbcnMHwUjS9FOOlsEXcnMGYEurdpfV0eAfW4uJi5OXlNd4PCAjAjz/+iMWLF+Pjjz+Gj48P1qxZg1mzZnX0SxMREVErTQn1xsRgL4PowCqTbo0mNWAqlQoKhQKVlZVwdnYWXQ4RERG1QmuP31woj4iIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRiGCEiIiKhGEaIiIhIKIYRIiIiEophhIiIiIRqUxiJi4tDWFgYnJ2d4ezsjMjISOzcubPF/RMTEyGTyW67ZWdn33PhREREZBqs2rKzr68v3nnnHfTp0wcA8Pnnn2PmzJlITU1FSEhIi887e/YsnJ2dG++7u7u3s1wiIiIyNW0KI1FRUU3uv/3224iLi8ORI0fuGEY8PDzg4uLSrgKJiIjItLV7zIhWq8WWLVtQU1ODyMjIO+47aNAgeHt7Y/z48di/f/9d/+36+nqoVKomNyIiIjJNbQ4jGRkZcHR0hFwuR3R0NLZt24bg4OBm9/X29sa6desQHx+PrVu3IjAwEOPHj0dycvIdXyM2NhYKhaLxplQq21omERERGQmZJElSW56gVquRl5eH69evIz4+Hv/+97+RlJTUYiD5raioKMhkMiQkJLS4T319Perr6xvvq1QqKJVKVFZWNhl7QkRERIZLpVJBoVDc9fjdpjEjAGBjY9M4gDUiIgLHjx/Hhx9+iE8++aRVzx8xYgQ2b958x33kcjnkcnlbSyMiIiIjdM99RiRJanIW425SU1Ph7e19ry9LREREJqJNZ0aWL1+OqVOnQqlUoqqqClu2bEFiYiJ27doFAFi2bBkKCwuxadMmAMDq1avh7++PkJAQqNVqbN68GfHx8YiPj+/4d0JERERGqU1hpLS0FE899RSKi4uhUCgQFhaGXbt2YeLEiQCA4uJi5OXlNe6vVquxZMkSFBYWws7ODiEhIdixYwemTZvWse+CiIiIjFabB7CK0NoBMERERGQ4Wnv85to0REREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJBTDCBEREQnFMEJERERCMYwQERGRUAwjREREJFSbwkhcXBzCwsLg7OwMZ2dnREZGYufOnXd8TlJSEoYMGQJbW1v06tULa9euvaeCiYiIyLS0KYz4+vrinXfeQUpKClJSUvDAAw9g5syZyMzMbHb/3NxcTJs2DWPGjEFqaiqWL1+OhQsXIj4+vkOKJyIiIuMnkyRJupd/wNXVFe+++y7+8Ic/3PbY0qVLkZCQgKysrMZt0dHRSE9Px+HDh1v9GiqVCgqFApWVlXB2dr6XcomIiKiLtPb43e4xI1qtFlu2bEFNTQ0iIyOb3efw4cOYNGlSk22TJ09GSkoKGhoaWvy36+vroVKpmtyIiIjINLU5jGRkZMDR0RFyuRzR0dHYtm0bgoODm923pKQEnp6eTbZ5enpCo9GgvLy8xdeIjY2FQqFovCmVyraWSUREREaizWEkMDAQaWlpOHLkCF544QXMnTsXZ86caXF/mUzW5P6tq0K/3f5ry5YtQ2VlZeMtPz+/rWUSERGRkbBq6xNsbGzQp08fAEBERASOHz+ODz/8EJ988slt+3p5eaGkpKTJtrKyMlhZWcHNza3F15DL5ZDL5W0tjYiIiIzQPfcZkSQJ9fX1zT4WGRmJvXv3Ntm2Z88eREREwNra+l5fmoiIiExAm8LI8uXLceDAAVy6dAkZGRl47bXXkJiYiNmzZwPQX155+umnG/ePjo7G5cuXERMTg6ysLGzYsAHr16/HkiVLOvZdEBERkdFq02Wa0tJSPPXUUyguLoZCoUBYWBh27dqFiRMnAgCKi4uRl5fXuH9AQAB+/PFHLF68GB9//DF8fHywZs0azJo1q2PfBRERERmte+4z0hXYZ4SIiMj4dHqfESIiIqKOwDBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVAMI0RERCQUwwgREREJxTBCREREQjGMEBERkVBWogsgIiI9jVaHM8UqlKrqYWUhg6WF7P//aymDpYUFrCxkULraQ2FnLbpcog7DMEJEJEi9RouMgkocza3A0dwKnLhUgRq19q7Pc7CxxMpHBmDmwB5dUCVR52MYISLqIjfUWqTmXbsZPq4iNe866jW6Jvs421ohwN0RkiRBo5Wg1UnQ6HTQ6PT3bzRoUVGjxqItaThysQIrooJha20p6B0RdQyGESKiTlKr1uBobgWO3bydKriOBq3UZB83BxsM7+WKYf6uGBbghiAvJ1hYyFr8N7U6CR/+nIN/7svBV8fykJp3DXFzhiCgu0Nnvx2iTiOTJEm6+25iqVQqKBQKVFZWwtnZWXQ5REQtkiQJJ/Ou4duUAvz3VDGq6zVNHvdytsXwXq4YHuCGYQGu6O3uAJms5fDRkoM55Xjp61SUV6sxopcrtvwpsqPeAlGHae3xm2dGiIg6QJmqDvEnC/HtiXxcvFLTuL2Hix1G9tYHj+EBblC62rUrfPzW6L7dsXbOEPxu7WFcKq+953+PSKQ2hZHY2Fhs3boV2dnZsLOzw8iRI7Fq1SoEBga2+JzExESMGzfutu1ZWVkICgpqe8VERAZCrdFhX3YpvkkpQNK5K9Dq9Cea7awtMW2ANx6L8MWwANcOCR/NcXOUAwBqfnP2hcjYtCmMJCUlYf78+Rg6dCg0Gg1ee+01TJo0CWfOnIGDw52vV549e7bJKRp3d/f2VUxEJFhWsQrfphTg+7RCVNSoG7dH9OyGRyN8MT3MB47yzj/xfOs1qtUa6HTSHceaEBmyNv227Nq1q8n9jRs3wsPDAydOnMDYsWPv+FwPDw+4uLi0uUAiIkNQWduAH9IL8W1KATIKKxu3ezjJMWuIL343xBe93R27tKZbYUSSgNoGbZcEIKLOcE8/uZWV+l9IV1fXu+47aNAg1NXVITg4GH/961+bvXRzS319Perr6xvvq1SqeymTiKhdtDoJh86X45uUfOw5Uwr1zWm41pYyTOjviccilBjTtzusLMU0s/711Z9atYZhhIxWu39yJUlCTEwMRo8ejdDQ0Bb38/b2xrp16zBkyBDU19fjiy++wPjx45GYmNji2ZTY2Fi8+eab7S2NiOieXL5ag+9OFOC7EwUorqxr3B7k5YTHIpR4aFAPuDrYCKxQb8+ZUgD6QbLdHeSCqyFqv3ZP7Z0/fz527NiBgwcPwtfXt03PjYqKgkwmQ0JCQrOPN3dmRKlUcmovEXWq9PzriEu8gN1nSnDrm1FhZ42HBvrg0QglQnycO20wanvM23gMiWevYMEDffDypJYnEhCJ0qlTexcsWICEhAQkJye3OYgAwIgRI7B58+YWH5fL5ZDLmfKJqPNJkoRD568iLuk8Dp2/2rh9TN/ueHyoEhP6expkh9MyVR2Sz10BADw8iG3hybi1KYxIkoQFCxZg27ZtSExMREBAQLteNDU1Fd7e3u16LhFRR9DqJOzJLEFc0gWcKtCPf7OykGHGQB+8cF9v9PV0Elzhnf2QVgSdBAz2c0GvLh44S9TR2hRG5s+fjy+//BI//PADnJycUFJSAgBQKBSws7MDACxbtgyFhYXYtGkTAGD16tXw9/dHSEgI1Go1Nm/ejPj4eMTHx3fwWyEiuju1RofvUwuxNvlCY3MyW2sLPDHUD8+NCYBvN3vBFbZO/MkCAMAjg9t+dprI0LQpjMTFxQEA7r///ibbN27ciHnz5gEAiouLkZeX1/iYWq3GkiVLUFhYCDs7O4SEhGDHjh2YNm3avVVORNQGNfUafHUsD/8+kIsSlX5QqrOtFeaO9Me8kf6NDcSMQWZRJbJLqmBjaYGoMB/R5RDdM65NQ0QmraJGjc9+uYTPf7mEyhsNAABPZzmeG90LTw73M8rpsAu+SsX29CJMG+CFf80eIrocohZxbRoiMmtF12/g0wMXseVYPm40aAEAAd0d8PzYXnh4cA/IrQxvUGprpOZdw/b0IshkwPxxfUSXQ9QhGEaIyKScL6vG2qQL+D61EJqba8WE+Djjz/f3wZRQL1gacct0SZKw8scsAMCswb4I8VEIroioYzCMEJFJSMu/jrjE89hzprSxR0hkLze8cH9vjOnb3aD6g7TXnjOlOH7pGmytLfDypH6iyyHqMAwjRGS0JEnCwfPliEu8gF8u/H+PkEnBnoi+vzcG+3UTWF3HatDq8M7ObADAc6N7wVthJ7gioo7DMEJERkerk7A7swRxiRcaF62zspBh5sAeiL6vl8H3CGmPL4/mIbe8Bt0dbRB9f2/R5RB1KIYRIjIaWp2ErScLEJd4ARfLjbdHSFup6hqw+qdzAICXJvQzyhlARHfCn2giMniSJGHvmVK8u/sscsqqAeh7hMwb6Y+5RtYjpD3+tf8CrtU2oLe7A54YqhRdDlGHYxghIoN2/FIF3tmZjROXrwHQL1z3wv29MWdET7M4Q3C+rBobDuYCAJZN7Q8rSwvBFRF1PNP/TSYio3S2pArv7s7GT1llAPSXY54ZFYDo+3pDYWctuLquIUkSXtuWAbVWh/sD3TG+v4fokog6BcMIERmUwus38MHec9h6sgA6CbC0kOGxCCVemtAXns62osvrUt+dKMDR3ArYWlvgf2eGmsT0ZKLmMIwQkUG4XqvGvxIv4LNfLkGt0QEApoR4YcnkQPTxML9VaStq1I0NzhZP6Aelq2kOziUCGEaISLAbai02/pKLuMQLqKrTAACGB7hi6dQgk+oT0lZv78jCtdoGBHk54dnRAaLLIepUDCNEJIRGq8O3Jwqw+qdzKFXVAwCCvJywdGoQ7u/nbtaXJH65UI74kwWQyYCVjwyANQetkoljGCGiLiVJ+oZlf999Fhev6HuF9HCxw8uT+mHmwB5GvXZMR6hr0OK1bacBAHOG9zTrs0NkPhhGiKjLHLl4Fe/szEZa/nUAQDd7a7z4QF/MGeFntKvodrR/JV5AbnkN3J3k+MuUQNHlEHUJhhEi6nRZxSr8fVc29p+9AgCws7bEc2MC8MexveBsax7TdFvjfFk14hLPAwDeiArhZ0Nmg2GEiDpNfkUtPth7DtvSCiFJ+vVjnhimxMIH+sLDzKbp3s2tniINWgnjAt0xbYCX6JKIugzDCBF1uIoaNT7adx6bj1yGWqufpjs9zBtLJgUioLuD4OoM09aThY09Rf7GniJkZhhGiKjD1Ko1WH8gF58kX0R1vX6a7sjebnh1ahDCfF3EFmfArtWo8fbNniKLxrOnCJkfhhEiumcNWh22HM/Hmp9zcKVKP0032NsZr04Nwpi+3flX/l2s2pWNiho1+nk64rkx7ClC5odhhIjaTZIk7Mgoxnu7z+LS1VoAgNLVDksmBSIqzAcWZj5NtzWOX6rAluP5AIC3H2ZPETJPDCNE1C6Hzpdj1a5snCqoBAC4Odhg4fi+eHKYH2yseEBtjQatDq9tywAAPB6hxFB/V8EVEYnBMEJEbXK2pApv7TiDAznlAAAHG0s8N6YX/ji2Fxzl/Eppi/UHc3GutBquDjZ4dWqQ6HKIhOE3BxG1SuWNBqz+6Rw2Hb4MrU6CtaUMs4f3xIsP9EF3R7no8oxOfkUtVv90DgCwfFp/dHOwEVwRkTgMI0R0RzqdhO9OFmDVzmxcrVEDACaHeGL5tP7o6cZpuu0hSRJWJGSirkGH4QGumDW4h+iSiIRiGCGiFp0quI7Xf8hsbN/ey90Bb0SFYGw/d7GFGbndmaXYl10Ga0sZ3n54AGcbkdljGCGi21ytrse7u8/i65R8SJJ+XMiiCX0xb2QAB6feI7VGh5U3e4o8P7Y3+ng4Cq6ISDyGESJqpNHq8OWxPLy3+yxUdfqmZQ8P6oFXpwbBk+3bO8R/jl5GXkUt3J3k+PO43qLLITIIDCNEBAA4lluB1384jeySKgBAf29nvDkjBMMCON20o6jqGrDm5xwAwOIJ/WBvw69gIoBhhMjslarqEPtjFr5PKwIAKOyssWRSPzw5zA9WbMDVoT5JuoBrtQ3o7e6AxyJ8RZdDZDAYRojMlFqjw8ZDuVjzcw5q1FrIZMATQ/3wl8mBcOU00w5XUlmH9QdzAQBLpwQx6BH9CsMIkRlKPncFb2zPxMUrNQCAgUoX/G1mCBez60Qf7D2HugYdInp2w8RgT9HlEBkUhhEiM5JfUYu3dpzB7sxSAEB3RxssnRKEWYN9uY5MJzpXWoVvT+jXn1k2rT+n8hL9BsMIkRmoa9BibdIFxCVeQL1GB0sLGeZG+uOliX3hbGstujyTt2pnNnQSMCXEC0N6dhNdDpHBYRghMmGSJGHPmVL873/PoODaDQBAZC83vDEjBIFeToKrMw9HLl7Fz9llsLSQ4S9TAkWXQ2SQGEaITNSFK9V4c/sZJJ+7AgDwVtjiten9MX2ANy8TdBFJkhB7s8HZk8OU6O3OBmdEzWEYITIx1fUa/HNfDjYczEWDVoKNpQX+ODYA88f1YV+LLpZ47grSCyphb2OJReP7iS6HyGDxm4nIREiShIT0Iqz8MQulqnoAwLhAd6yICoF/dy5oJ8J/juQBAB4fqoS7E1c2JmoJwwiRCcgqVmHFD5k4dqkCANDTzR6vPxiM8f05hVSU4sob2Jetn7U0e3hPwdUQGTaGESIjVlnbgH/sPYsvjlyGTgJsrS3w4rg+eG5ML9haW4ouz6xtOZYPnQQMD3DlYnhEd8EwQmSEdDoJ36Tk4++7z6KiRg0AmD7AG8un90cPFzvB1ZFGq8PXx/V9RX4/3E9wNUSGj2GEyMik5V/Hih9OI72gEgDQ18MRb8wIwag+3QVXRrfsyy5DiaoOrg42mBLqJbocIoPHMEJkJMqr6/H3Xdn4JqUAAOAot8JLE/pi7kh/WHOdE4Py5TH9wNVHh/hCbsXLZUR3wzBCZOA0Wh2+OHIZ/9h7DlV1GgDArMG+WDo1EB5OtoKro9/Kr6hF0s3eLk8O4yUaotZgGKFOp9VJOJZbgbKqOng42WJYgCssuQ5Kqxy5eBVvJGQiu6QKABDawxlvzgjBkJ6ugiujlmw5ngdJAkb36c4p1feA3xvmpU1hJDY2Flu3bkV2djbs7OwwcuRIrFq1CoGBd25xnJSUhJiYGGRmZsLHxwevvPIKoqOj76lwMg67Thfjze1nUFxZ17jNW2GLFVHBmBLqLbAyw1amqsP/7sjC9vQiAICLvTX+MjkQTwz14xeyAWvQ6vD1cf1ltNkcuNpu/N4wP2260JyUlIT58+fjyJEj2Lt3LzQaDSZNmoSampoWn5Obm4tp06ZhzJgxSE1NxfLly7Fw4ULEx8ffc/Fk2HadLsYLm082+UIBgJLKOryw+SR2nS4WVJnh0ukkfHUsD+P/kYTt6UWwkAFzRvhh/8v3Y/bwngwiBm7vmVKUV9fD3UmOCcHs8dIe/N4wT206M7Jr164m9zdu3AgPDw+cOHECY8eObfY5a9euhZ+fH1avXg0A6N+/P1JSUvDee+9h1qxZ7auaDJ5WJ+HN7WcgNfOYBEAG4M3tZzAx2IsH2JsuXqnGsq0ZOJqrb1wW7qvA2w8PQGgPheDKqLX2Z5cBAB4Z1IODituB3xvm657GjFRW6qcWurq2fP368OHDmDRpUpNtkydPxvr169HQ0ABr69uXL6+vr0d9fX3jfZVKdS9lkgDHcitu+8vm1yQAxZV1OJZbgcjebl1XmAFq0OqwLvkiPvw5B2qNDnbWlnh5Uj88MyqAX7hGJrNI/101uGc3wZUYJ35vmK92hxFJkhATE4PRo0cjNDS0xf1KSkrg6dn0dKWnpyc0Gg3Ky8vh7X379b/Y2Fi8+eab7S2NDEBZVctfKO3Zz1Sl51/H0vhTjQNUx/TtjpUPD4DS1V5wZdRW9RotzpXq/z+G+DgLrsY48XvDfLU7jLz44os4deoUDh48eNd9f7tcuSRJzW6/ZdmyZYiJiWm8r1KpoFQq21sqCdDaKafmOjW1Vq3B+3vOYeOhXOgkoJu9NV6PCsZDA3u0+HtBhi2ntBoanQQXe2t2wW0nfm+Yr3aFkQULFiAhIQHJycnw9fW9475eXl4oKSlpsq2srAxWVlZwc2v+NJtcLodczhUujdmwAFd4K2xRUlnX7PVfGQAvhX66nrlJOncFr23LQMG1GwCAhwb64H8eDIabI3/mjdnpQv1l6xAfZwbKduL3hvlq0wgrSZLw4osvYuvWrdi3bx8CAgLu+pzIyEjs3bu3ybY9e/YgIiKi2fEiZBosLWRYERUMQP8F8mu37q+ICjarMREVNWrEfJ2GuRuOoeDaDfRwscPGZ4Zi9RODGERMwK3xIqE+HHDcXvzeMF9tCiPz58/H5s2b8eWXX8LJyQklJSUoKSnBjRs3GvdZtmwZnn766cb70dHRuHz5MmJiYpCVlYUNGzZg/fr1WLJkSce9CzJIU0K9ETdnMLwUTU+peilsETdnsNn0C5AkCd+nFmLCP5KwNbUQMhnwzCh/7Fk8FuMCPUSXRx3kdJH+zEgwx4vcE35vmCeZdGsAR2t2buHU48aNGzFv3jwAwLx583Dp0iUkJiY2Pp6UlITFixc3Nj1bunRpm5qeqVQqKBQKVFZWwtmZv+jGxpw7KRZcq8Vr2043tgcP9HTCO7MGYJAfZ1uYEq1OQsiKXahr0OHnl+9Db3dH0SUZPXP+3jAlrT1+tymMiMIwQsZGq5Pw+S+X8N6es6hVa2FjaYGF4/vgT2N7w8aK/SdMTU5pFSZ+kAx7G0ucfmMyLHjQJALQ+uM316Yh6mDZJSosjc9Aev51AMAwf1esfGQA+njwr2VTdWu8SH9vZwYRonZgGCHqIHUNWny07zzWJl2ARifBSW6FV6cF4cmhfjxAmbhbM2lCOV6EqF0YRog6wNGLV7FsawYuluvXaZoU7Im/zQy9bRAemaZrtQ0AAA9n/v8mag+GEaJ7oKprwDs7s/Hl0TwAgLuTHP87M4Qj/s1Md0cbAMDVarXgSoiME8MIUTvtzizB6z+cRqlKv47Sk8OUeHVqfyjs2D/H3HS/2SemvLr+LnsSUXMYRojaqExVhxUJmdh5Wt9ZOKC7A1Y+PIALd5mx7k76MyMMI0TtwzBC1EqSJOHr4/l4+8csVNVpYGUhw/P39cKCB/rC1tpSdHkkEM+MEN0bhhGiVrh4pRrLtmbgaG4FACDMV4F3Hgljt00C8OswwjEjRO3BMEJ0Bw1aHdYlX8SHP+dArdHBztoSL0/qh2dGBbAbJDW6FUau1aqh0epgZcnGdkRtwTBC1IL0/OtYGn8K2SVVAIAxfbtj5cMDoHS1F1wZGRpXBxtYyACdpF8QkVN8idqGYYToN2rVGvxjzzlsOJQLnQS42Fvj9QeD8fCgHlwanpplaSGDq4MNyqvVKK9mGCFqK4YRol9JOncFr23LQME1/UrUMwf64H8eDG48DU/Uku6O8pthhINYidqKYYQI+lPrb/33DLamFgIAerjY4a2HQzEu0ENwZWQs9IG1imGEqB04yorMmiRJ+CGtEBP+kYStqYWQyYBnRvljz+KxDCLUJj3d9GOJks5dEVwJkfFhGCGzVXCtFs98dhyLtqShokaNQE8nbH1hJFZEhcBBzpOG1DZPDvMDAOw4VYziyhuCqyEyLgwjZHa0OgkbDuZi0gfJSDx7BTaWFnh5Yj9sXzAag/y6iS6PjFRoDwVG9HKFRifh818uiy6HyKjwzz8yK9klKiyNz0B6/nUAwDB/V6x8ZAD6eDiKLYxMwh9G98KRixX48uhlLHigD8+wEbUSf1PILNQ1aPHx/vOIS7wAjU6Ck9wKr04LwpND/WDB5mXUQcYHecDfzR6XrtbiuxMFmDvSX3RJREaBl2nI5B3LrcC0NQfwz33nodFJmBjsib0x92H28J4MItShLCxk+MPoAADAxkO50OokwRURGQeGETJZVXUNeG1bBh775DAuXqmBu5MccbMHY91TQ+ClYFMq6hyzhvhCYWeNS1dr8XNWqehyiIwCwwiZpF8ulGPK6gP4z9E8AMCTw5T4afF9mDrAm11UqVPZ21hh9nD9zJp/H8wVXA2RceCYETIpdQ1arNqVjY2HLgEAlK52WDUrDCN7dxdbGJmVuSP98emBiziWW4FTBdcR5usiuiQig8YzI2Qy0vKvY9qaA41B5Mlhfti5aCyDCHU5T2dbPBjmAwBYz7MjRHfFMEJGT63R4f09ZzEr7hdcvFIDDyc5Ns4bithHBsCRUytJkFsDWdkEjejuGEbIqJ0tqcLD/zqEf+47D61OwoxwH30r9yC2ciexft0Ebcm36WjQ6kSXRGSwGEbIKGl1EtYmXUDUPw8is0gFF3trfPT7QVjz5CC42NuILo8IAPD6gyGwt7HEofNX8dq2DEgSp/oSNYdhhIzO5as1ePyTw3hnZzbUWh0eCPLAnpfGNl6jJzIUwT7O+Pj3g2EhA75JKcC/Ei+ILonIIDGMkNGQJAmbj1zG1A8PIOXyNTjYWOLvs8Kwfm4EPJzZN4QM07ggD7w5IwQA8O7us/ghrVBwRUSGh6P7yCiUVNbhlfhTSL65PPvwAFe892g4lK72gisjurunIv1x+Wot/n0wF3/59hR8XOww1N9VdFlEBoNnRsigSZKE71MLMemDJCSfuwK5lQX+58FgfPXHEQwiZFSWTeuPySGeUGt1+NOmFFwqrxFdEpHBYBghg3W1uh5//s9JvPR1GlR1GoT7KrBj4Rj8YXQA15Qho2NpIcPqxwch3FeBa7UNeOaz47hWoxZdFpFBYBghg7T3TCkmr07GztMlsLKQIWZiP8S/MBJ9PBxFl0bUbnY2lvj33KHo4WKH3PIa/OmLFNQ1aEWXRSQcwwgZFFVdA5Z8m44/bkpBebUa/Twd8f38UVg4vi+sLPnjSsbP3UmOz54ZCidbKxy/dA2vfHeKU37J7PHbnQzGL+fLMXX1AXx3ogAyGfD82F5IeHE0QnsoRJdG1KH6ejph7ZwhsLKQISG9CP/Ye050SURCMYyQcDfUWryRkInf//soCq/fgJ+rPb7+UySWTesPW2tL0eURdYpRfbpj5SMDAAD/3Hce36TkC66ISBxO7SWhUvOu4eVv0nHx5syC2cP9sHxafzhwTRkyA49FKJF3tRYf7T+P5VszILeywMyBPUSXRdTl+I1PQqg1Oqz5OQf/SjwPnQR4OsuxalYY7g/kmjJkXl6e1A/512rxQ1oRFm1Jw7HcCvzPg8E8K0hmhWGEulx2iQoxX6fjTLEKADBzoA/+NiMUCntrwZURdT2ZTIb3Hw2Hsps9Pk48j/8czUNq3nX8a/Zg+Hd3EF0eUZeQSUYwjFulUkGhUKCyshLOzs6iy6F20uokrEu+iA/2noNaq0M3e2u8/fAATBvgLbo0IoOQdO4KFn+dhooaNRzlVlg1KwzTw/j7QcartcdvhhHqEpfKa/Dyt+k4cfkaAGBCfw+sfGQAPJy4pgzRrxVX3sDCr1Jx/JL+d2VuZE8sn94fcitetiHjwzBCBuHW4nYrf8zGjQYtHOVWeD0qGI8O8YVMxi6qRM3RaHV4f+85xN1c5TfMV4GPnhwMPzcugUDGhWGEhCuuvIFXvjuFAznlAIDIXm5499Ew+HbjFypRa+zPLsPib9JwvbYBTrZWePd34ZgS6iW6LKJWYxghYSRJwrbUQqxIyERVnQZyKwu8OjUIcyP9uaYMURsVXr+BBV+exMm86wCAZ0cF4NWpQbCxYpsoMnwMIyTE1ep6vLbtNHZllgAAwpUueP/RcK4pQ3QPGrQ6/H1XNj49kAtA/3v18e8H8SwjGTyGEepyezJLsHxbBsqr1bCykOGlCX0RfV9vrilD1EH2ninFy9/oV7FW2Fnj/UfDMSHYU3RZRC1q7fG7zUeJ5ORkREVFwcfHBzKZDN9///0d909MTIRMJrvtlp2d3daXJgOlqmvAy9+k409fnEB5tRqBnk74fv4ovPgAF7cj6kgTgz2xY+EYhCtdUHmjAc9tSsHKH7PQoNWJLo3onrT5SFFTU4Pw8HB89NFHbXre2bNnUVxc3Hjr27dvW1+aDNCh8+WY8kEy4k/eXNzuvl5IWDCKi9sRdRKlqz2+fT4Sz44KAACsS76Ixz85jKLrNwRXRtR+be7AOnXqVEydOrXNL+Th4QEXF5c2P48M0w21Fu/szMLnhy8DAHq62eP9R8MR4e8quDIi02djZYHXo4IxLMAVf/kuHSfzrmP6mgNYOiUIj0YoYcmB4mRkuuwc+qBBg+Dt7Y3x48dj//79d9y3vr4eKpWqyY0Mx8m8a5i25kBjEJkzwg8/LhzDIELUxaaEemHHgjEY0EOBa7UNeHVrBqZ9eAD7z5bBCIYDEjXq9DDi7e2NdevWIT4+Hlu3bkVgYCDGjx+P5OTkFp8TGxsLhULReFMqlZ1dJrWCWqMf0f+7uF+QW14DL2dbbHp2GN56aABX2SUSxM/NHvEvjMRfp/eHws4aZ0ur8MzG45iz/igyiypFl0fUKvc0m0Ymk2Hbtm146KGH2vS8qKgoyGQyJCQkNPt4fX096uvrG++rVCoolUrOphEoq1iFxV+nIbukCgDw8KAeeCMqhIvbERmQytoGfLQ/B5//chlqrQ4ymf53dcmkQPi42Ikuj8xQp82m6QgjRoxATk5Oi4/L5XI4Ozs3uZEYGq0OH+8/jxkfHUR2SRVcHWywds5gfPD4QAYRIgOjsLfGa9OD8fPL9yEq3AeSBGw9WYhx7yXi77uyUVXXILpEomYJCSOpqanw9uZKlIYut7wGj31yGO/uPosGrYSJwZ7Y/dJYTAnl/zsiQ6Z0tcc/nxyE7+ePwjB/V9RrdPhX4gXc/24iNh2+xKnAZHDafKG/uroa58+fb7yfm5uLtLQ0uLq6ws/PD8uWLUNhYSE2bdoEAFi9ejX8/f0REhICtVqNzZs3Iz4+HvHx8R33LqhD6XQSNh+9jNibi9s5ya2wYkYIZg3uwcXtiIzIQKULvn5+BPaeKcU7O7NxsbwGr/+Qic8OXcLSqUGYFOzJ32kyCG0OIykpKRg3blzj/ZiYGADA3Llz8dlnn6G4uBh5eXmNj6vVaixZsgSFhYWws7NDSEgIduzYgWnTpnVA+dTRiq7rF7c7eF6/uN2oPm74++/C0YPXm4mMkkwmw6QQL4wL8sCWY3lY/VMOLpbX4PkvTmCYvyuWT++PgUoX0WWSmWM7eAKgX9xu68lCvLFdv7idrbUFlk3tj6dG9OTidkQmpKquAWuTLuDfB3JRr9FfrokK98ErkwOhdOVaN9SxuDYNtVp5dT2Wb83AnjOlAIBBfvrF7Xq5c3E7IlNVdP0G3t9zDltTCyBJgI2lBZ6O7IkXH+gDF3sb0eWRiWAYoVbZnVmC5VszcLVGDWtLGV6a0A/Pj+3FNWWIzERmUSVif8xuvDSrsLPGggf64KnInpBbWQqujowdwwjd0Q21Fv+74wy+PKof3xPk5YR/PDYQwT78fInMjSRJSDp3BbE/ZuNsqb6XkNLVDq9MDsKDYd4c5ErtxjBCLcouUWHBl6nIKasGADw/thdiJvXjX0FEZk6rk/DdiXy8v+ccyqr0jSfDlS54bVp/DAvgcg/UdgwjdBtJkvDFkct4a0cW1Bod3J3k+OCxgRjdt7vo0ojIgNSqNfg0ORefJF9ArVoLABjq3w3PjgrAxGBPXsalVmMYoSau1ajxl+9O4acs/SDVcYHueO/RcLg5ygVXRkSGqqyqDh/szcG3KfnQ6PSHih4udpg30h+PD1PC2ZZdmOnOGEao0eELV7H46zSUqOpgY2mBV6cG4ZlR/rwOTEStUqqqwxeHL+M/Ry/jWq2+pbyDjSUejVBi3kh/+Hd3EFwhGSqGEYJGq8OHP+fgo/3nIUlAL3cHrHliEEJ7KESXRkRGqK5Bi+9TC7HhUC7OlerHnMlkwPggDzw7OgCRvdz4Rw41wTBi5vIrarFoSypO5l0HADweocSKGcGwt2lz010ioiYkScKh81ex/uBF7D97pXF7kJcTnh0dgBnhPrC15oB4Yhgxa9vTi7B8Wwaq6jRwklth5SMDEBXuI7osIjJBF65U47NDl/DdiQLcaNAPdnVzsMHsET0xZ4QfPJxsBVdIIjGMmKFatQZvJGTim5QCAMBgPxd8+MQgtngmok5XWduAr47n4fNfLqG4sg6AvqtrVLgPnh3tjxAfXh42RwwjZuZ0YSUWbknFxSs1kMmAF8f1waLxfTkFj4i6VINWh92ZJdhwMLfxMjEADA9wxbOjAzChvycsud6V2WAYMROSJGHDoUtYtTMbaq0OXs62+ODxgYjs7Sa6NCIyc6l517Dh0CX8mFEM7c2pwX6u9pg30h+PRvjCiVODTR7DiBkor67HX75NbxxANjHYE3+fFYZuDlzkiogMR3HlDWw6fBlfHs1D5Q391GBHuRUei1DimVH+vJRswhhGTNyBnCuI+SYdV6rqYWNlgf+Z3h9zRvTktDoiMlg31FpsTS3AhoO5uHClBgBgIdP/IfXsqAAMC3Dld5iJYRgxUWqNDu/vPYtPki4CAPp5OmLNk4MQ5GXenwsRGQ+dTkJyzhVsOHQJyef+f2pwiI8znh0VgAfDvblWlolgGDFBl6/WYOFXqUgvqAQAzB7uh79OD4adDX9picg45ZRWYeMvl7D1ZAHqGnQAAHcnOZ4a0RO/H+6H7lyywqgxjJiYbakF+Ou206hRa6Gws8aqWQMwJdRbdFlERB3iWo0aXx7Lw6bDl1Cq0q8YbGNlgRnhPnhkcA8MD3DjLBwjxDBiIqrrNXj9+9PYmloIABjm74rVTwyEj4ud4MqIiDpeg1aHHzOKseFgbuNZYEB/tmT6AG/MGOiDQUoXji0xEgwjJuBUwXUs/CoVl67WwkIGLBrfDy8+0Id/HRCRyZMkCSfzruHblALsPF3SOAsH0K8cHBXug6hwbwR7OzOYGDCGESOm00n49MBFvLv7LDQ6CT1c7LD6iYEY6u8qujQioi6n1uhw8PwVJKQVYe+ZUtSotY2P9XZ3uBlMfNDb3VFgldQchhEjVVZVh5e/SceBnHIAwLQBXoh9OAwKezYHIiK6odZi/9kyJKQVYd/ZMqg1usbHQnycERXugwfDvOHbjb1LDAHDiBHaf7YMS75Jx9UaNWytLfBGVAgeH6rkKUgiomZU1TVg75lSbE8vwoGccmh0/384G9KzG6LCvDEtzJuL9QnEMGJE6jVa/H3XWaw/mAtAvwz3R78fhD4eToIrIyIyDhU1auw6XYLt6UU4knsVt45sFjIgsrcbosJ8MCXUCy727FDdlRhGjMSFK9VY+FUqMotUAIB5I/3x6tQg2FqzdwgRUXuUquqw41Qxtp8qQuqvFuuzspBhbD93zAj3wYRgTzjKrcQVaSYYRgycJEn49kQBVvyQiRsNWnSzt8a7vwvHhGBP0aUREZmM/IpabD9VhO3pxcgqVjVul1tZYHx/D8wI98H9gR78A7CTMIwYMFVdA17bdhrb04sAAJG93LD6iYHwdOZ1TSKiznK+rAoJ6cXYnl6E3PKaxu2OcitMCvZE1EAfjO7THdaWFgKrNC0MIwbqxOVrWLQlFQXXbsDSQoaYif0QfV9v9g4hIuoikiQhs0iF7elF2J5ehKLKusbHXOytMTXUG1Hh3uz62gEYRgyMVichLvE8PvgpB1qdBKWrHT58YhAG+3UTXRoRkdnS6SSk5l/D9vRi/PdUMcqr6xsf83CSY3qYN6LC2fW1vRhGDEhJZR0Wf52GwxevAgBmhPvgrYdD4WzL3iFERIZCo9XhaG4FtqcX3db11bfbza6vYT7o7+3EYNJKDCMG4qczpfjLd+m4VtsAextL/G1mKGYN7sEfZCIiA6bW6HAg5wq2pxdhz5lS1P6m6+uM8B6ICvdGL3Z9vSOGEcHqGrSI/TELnx++DAAI7eGMNU8M4g8uEZGRuaHWYl92GbanN9/1dUa4Dx4M90EPLmB6G4YRgXJKq7Dgq1Rkl1QBAP44JgBLJgdCbsWpY0RExqyqrgF7Mkux/VQRDjbT9XVGuA+mDvBi19ebGEYEkCQJXx3Lx9/+m4m6Bh26O9rgvUfDcX+gh+jSiIiog93q+pqQXoijuRXs+toMhpEudr1WjVfjM7ArswQAMLafO95/NBzuTnLBlRERUWe71fU1Ib0IafnXG7dbW8owtq87osJ9MDHYEw5m1vWVYaQLHcutwEtbUlFUWQdrSxlemRyEP4wOgAXnpxMRmZ1bXV8T0ooaL9cDgK21BcYHeSIq3Ntsur4yjHQBjVaHf+47j3/uy4FOAvzd7LHmyUEI83URXRoRERmAnNIqbD/VQtfXEE9EhZt211eGkU5WeP0GXtqSiuOXrgEAHhncA3+bGcqFl4iI6DZ36vrazd4aUwd4IyrMB8MCXE2q6yvDSCfamVGMpfGnoKrTwFFuhbceCsVDg3qILouIiIyATifhZN41bE8vwo6MYpRXqxsf83CS48EwH0SFe2OgCXR9ZRjpBDfUWvztv2fw1bE8AEC40gVrnhiInm4OwmoiIiLjpdHqcOTira6vxVDVaRofu9X1dUa4D4K8jLPrK8NIB8sqVmHhV6nIKauGTAZE39cbMRP7mex1PiIi6lpqjQ7J565g+6ki7P1N19c+Ho6IunnGxJiaZzKMdBBJkvDFkct4a0cW1Bod3J3k+OCxgRjdt3uX1kFERObjhlqLn7NLsT29CPvPXmnS9TW0hzOiwoyj6yvDSAeoqFHjle9O4aesUgDAA0EeePd3YXBzZO8QIiLqGqq6BuzNLEVCehEOni+H9lddXyN6dkNUuA+mDfA2yL5WDCP36JcL5Vj8dRpKVfWwsbTAsmlBmDfS3yiv2RERkWmoqFFj52n9VOHfdn0d2bs7osK9MSXEGwp7w1gVnmHkLrQ6CcdyK1BWVQcPJ9vG6VQNWh1W/3QO/0q8AEnSr8645slBCPFRdMjrEhERdYSSyjrsyNAHk992fb2vn77r64T+d+762tKxsKN0WhhJTk7Gu+++ixMnTqC4uBjbtm3DQw89dMfnJCUlISYmBpmZmfDx8cErr7yC6OjoVr9mR4eRXaeL8eb2Myj+1Txvb4Ut5o/rjfiThUjNuw4AeGKoEq9HBcPehr1DiIjIcOVd1Xd93Z7eTNfX/p6ICvPB/YHuTbq+tnQsXBEVjCmh3h1SV6eFkZ07d+LQoUMYPHgwZs2addcwkpubi9DQUPzxj3/E888/j0OHDuHPf/4zvvrqK8yaNatD30xr7DpdjBc2n8Sd3rSTrRViHxmAB8N87um1iIiIulpOaRW2pxchIb0Il67WNm6/1fV1RrgPauo1ePHL1NuOhbfOicTNGdwhgaRLLtPIZLK7hpGlS5ciISEBWVlZjduio6ORnp6Ow4cPt+p1OiqMaHUSRq/a1yQF/pa1pQw/xdzH3iFERGTUbnV9TUgvwn9/0/VVJgNaOvrLAHgpbHFw6QP3fMmmtcfvTm+ScfjwYUyaNKnJtsmTJyMlJQUNDQ3NPqe+vh4qlarJrSMcy624YxABgAathKLrd96HiIjI0MlkMoT2UGD5tP44uPQBfBcdiacje8LZzrrFIAIAEoDiyjocy63oslo7PYyUlJTA09OzyTZPT09oNBqUl5c3+5zY2FgoFIrGm1Kp7JBayqpaFzJaux8REZExsLCQIcLfFX+bGYo3ooJb9ZyuPBZ2SfvQ306HvXVlqKVpssuWLUNlZWXjLT8/v0Pq8HCy7dD9iIiIjI23onWN0rryWNjp00S8vLxQUlLSZFtZWRmsrKzg5ubW7HPkcjnk8o5v3jIswBXeCluUVNY1O4D11nWyYQGuHf7aREREhsAQj4WdfmYkMjISe/fubbJtz549iIiIgLV11zZlsbSQYcXN01O/PSdz6/6KqGCTWr6ZiIjo1wzxWNjmMFJdXY20tDSkpaUB0E/dTUtLQ16efiXbZcuW4emnn27cPzo6GpcvX0ZMTAyysrKwYcMGrF+/HkuWLOmYd9BGU0K9ETdnMLwUTU8/eSlsO2wqExERkSEztGNhm6f2JiYmYty4cbdtnzt3Lj777DPMmzcPly5dQmJiYuNjSUlJWLx4cWPTs6VLlwptegZ0ftc5IiIiQ2e0HVhFELlqLxEREbWPwfQZISIiIroThhEiIiISimGEiIiIhGIYISIiIqEYRoiIiEgohhEiIiISimGEiIiIhGIYISIiIqEYRoiIiEioTl+1tyPcahKrUqkEV0JEREStdeu4fbdm70YRRqqqqgAASqVScCVERETUVlVVVVAoFC0+bhRr0+h0OhQVFcHJyQkyWccu4KNUKpGfn881bzoZP+uuwc+5a/Bz7hr8nLtGZ37OkiShqqoKPj4+sLBoeWSIUZwZsbCwgK+vb6f9+87OzvxB7yL8rLsGP+euwc+5a/Bz7hqd9Tnf6YzILRzASkREREIxjBAREZFQZh1G5HI5VqxYAblcLroUk8fPumvwc+4a/Jy7Bj/nrmEIn7NRDGAlIiIi02XWZ0aIiIhIPIYRIiIiEophhIiIiIRiGCEiIiKhzDaMJCcnIyoqCj4+PpDJZPj+++9Fl2RyYmNjMXToUDg5OcHDwwMPPfQQzp49K7oskxMXF4ewsLDGhkWRkZHYuXOn6LJMXmxsLGQyGV566SXRpZicN954AzKZrMnNy8tLdFkmqbCwEHPmzIGbmxvs7e0xcOBAnDhxosvrMNswUlNTg/DwcHz00UeiSzFZSUlJmD9/Po4cOYK9e/dCo9Fg0qRJqKmpEV2aSfH19cU777yDlJQUpKSk4IEHHsDMmTORmZkpujSTdfz4caxbtw5hYWGiSzFZISEhKC4ubrxlZGSILsnkXLt2DaNGjYK1tTV27tyJM2fO4P3334eLi0uX12IU7eA7w9SpUzF16lTRZZi0Xbt2Nbm/ceNGeHh44MSJExg7dqygqkxPVFRUk/tvv/024uLicOTIEYSEhAiqynRVV1dj9uzZ+PTTT/HWW2+JLsdkWVlZ8WxIJ1u1ahWUSiU2btzYuM3f319ILWZ7ZoS6XmVlJQDA1dVVcCWmS6vVYsuWLaipqUFkZKTockzS/PnzMX36dEyYMEF0KSYtJycHPj4+CAgIwBNPPIGLFy+KLsnkJCQkICIiAo8++ig8PDwwaNAgfPrpp0JqYRihLiFJEmJiYjB69GiEhoaKLsfkZGRkwNHREXK5HNHR0di2bRuCg4NFl2VytmzZgpMnTyI2NlZ0KSZt+PDh2LRpE3bv3o1PP/0UJSUlGDlyJK5evSq6NJNy8eJFxMXFoW/fvti9ezeio6OxcOFCbNq0qctrMdvLNNS1XnzxRZw6dQoHDx4UXYpJCgwMRFpaGq5fv474+HjMnTsXSUlJDCQdKD8/H4sWLcKePXtga2sruhyT9utL6AMGDEBkZCR69+6Nzz//HDExMQIrMy06nQ4RERFYuXIlAGDQoEHIzMxEXFwcnn766S6thWdGqNMtWLAACQkJ2L9/P3x9fUWXY5JsbGzQp08fREREIDY2FuHh4fjwww9Fl2VSTpw4gbKyMgwZMgRWVlawsrJCUlIS1qxZAysrK2i1WtElmiwHBwcMGDAAOTk5oksxKd7e3rf9wdK/f3/k5eV1eS08M0KdRpIkLFiwANu2bUNiYiICAgJEl2Q2JElCfX296DJMyvjx42+b0fHMM88gKCgIS5cuhaWlpaDKTF99fT2ysrIwZswY0aWYlFGjRt3WbuHcuXPo2bNnl9ditmGkuroa58+fb7yfm5uLtLQ0uLq6ws/PT2BlpmP+/Pn48ssv8cMPP8DJyQklJSUAAIVCATs7O8HVmY7ly5dj6tSpUCqVqKqqwpYtW5CYmHjbbCa6N05OTreNd3JwcICbmxvHQXWwJUuWICoqCn5+figrK8Nbb70FlUqFuXPnii7NpCxevBgjR47EypUr8dhjj+HYsWNYt24d1q1b1/XFSGZq//79EoDbbnPnzhVdmslo7vMFIG3cuFF0aSbl2WeflXr27CnZ2NhI7u7u0vjx46U9e/aILsss3HfffdKiRYtEl2FyHn/8ccnb21uytraWfHx8pEceeUTKzMwUXZZJ2r59uxQaGirJ5XIpKChIWrdunZA6ZJIkSV0fgYiIiIj0OICViIiIhGIYISIiIqEYRoiIiEgohhEiIiISimGEiIiIhGIYISIiIqEYRoiIiEgohhEiIiISimGEiIiIhGIYISIiIqEYRoiIiEgohhEiIiIS6v8Ay12TuZPENpwAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 640x480 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "P = [(1,1), (6, 4), (5,2), (2, 2), (6,1)]\n",
    "\n",
    "bc = BézierCurve(P)\n",
    "\n",
    "# nb points de la courbe de Bézier\n",
    "n = 25\n",
    "t = [i/(n+1) for i in range(n)] + [1]\n",
    "\n",
    "b = [bc(_) for _ in t]\n",
    "\n",
    "plt.scatter([_[0] for _ in P], [_[1] for _ in P])\n",
    "plt.plot([_[0] for _ in b], [_[1] for _ in b])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a66df6c4",
   "metadata": {},
   "source": [
    "À part les points initiaux et finaux, la courbe de Bézier ne passe pas nécessairement par les points de contrôle."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6dc0723",
   "metadata": {},
   "source": [
    "### Surface de Bézier\n",
    "\n",
    "Les surfaces de Bézier sont une généralisation des courbes de Bézier, à trois dimensions. Elles utilisent des points de contrôles à trois dimensions $\\left(x,y,z\\right)$ et leur formule s'écrit\n",
    "\n",
    "$$\n",
    "\\mathbf{B}\\left(t\\right) = \\sum_{i=0}^{n} \\sum_{j=0}^{m} b_{n,i}\\left(u\\right) b_{m,j}\\left(v\\right) \\mathbf{P}_{i,j} \\;,\\; b_{n,i} \\left(t\\right) = {n\\choose i}(1 - t)^{n - i}t^i\n",
    "$$\n",
    "\n",
    "où il y a $n\\times m$ points de contrôles $\\mathbf{P}_{i,j} $ répartis sur une grille représentant les coordonnées $x$ et $y$, tandis que l'altitude $z$ donne la troisième dimension du point de contrôle. Il n'y a pas d'ordre dans l'arrangement des points en $x$ _et en_ $y$ : ces deux dimensions sont dissociées, et on parle de produit tensoriel d'espace dans ce cas. En revanche, l'ordre des points de passage en $x$ _ou_ en $y$ est déterminé."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a69a2498",
   "metadata": {},
   "source": [
    "![Surface de Bézier](fig/bezsurf.gif)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
