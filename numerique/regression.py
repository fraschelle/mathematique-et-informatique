#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Regression tools
"""
from typing import Sequence, Tuple
from math import sqrt

def linear_fit(x: Sequence[float], y: Sequence[float]) -> Tuple[float, float]:
    """
    Apply the least square method for a linear fit of a sample.
    see https://fr.wikipedia.org/wiki/R%C3%A9gression_lin%C3%A9aire

    Parameters
    ----------
    x : Sequence[float]
        Absissa of the dataset.
    y : Sequence[float]
        Ordinates of the dataset.

    Raises
    ------
    ValueError
        in case x and y do not have same length.

    Returns
    -------
    a : float
        slope of the linear affine regression y = a*x + b.
    b : float
        intercept of the linear affine regression y = a*x + b.    
    """
    if not len(x) == len(y):
        raise ValueError("x and y must have same length")
    xm = sum(x) / len(x)
    ym = sum(y) / len(y)
    numer = sum(x[i] * y[i] for i in range(len(x))) / len(x) - xm * ym
    denom = sum(_ * _ for _ in x) / len(x) - xm * xm
    a = numer / denom
    b = ym - a * xm
    return a, b

def correlation(x: Sequence[float], y: Sequence[float]) -> float:
    """Calculate the Pearson correlation coefficient between the x and y series.
    see https://fr.wikipedia.org/wiki/Corr%C3%A9lation_(statistiques)
    
    Parameters
    ----------
    x : Sequence[float]
        Absissa of the dataset.
    y : Sequence[float]
        Ordinates of the dataset.

    Raises
    ------
    ValueError
        in case x and y do not have same length.

    Returns
    -------
    c : float
        The Pearson correlation coefficient.
    """
    if not len(x) == len(y):
        raise ValueError("x and y must have same length")
    xm = sum(x) / len(x)
    ym = sum(y) / len(y)
    numer = sum(x[i] * y[i] for i in range(len(x))) / len(x) - xm * ym
    denom_x = sum(_ * _ for _ in x) / len(x) - xm * xm
    denom_y = sum(_ * _ for _ in y) / len(y) - ym * ym
    return numer / sqrt(denom_x * denom_y)

def root_mean_square(y_sample: Sequence[float], y_predicted: Sequence[float]) -> float:
    """Calculate the RMS value of a predicted value over a sample.
    see https://en.wikipedia.org/wiki/Root-mean-square_deviation
    
    Parameters
    ----------
    y_sample : Sequence[float]
        Ordinates of the sample dataset.
    y_predicted : Sequence[float]
        Ordinates of the prediceted dataset.

    Raises
    ------
    ValueError
        in case y_sample and y_predicted do not have same length.

    Returns
    -------
    RMS : float
        the root-mean squared error between y_sample and y_predicted
    """
    if not len(y_sample) == len(y_predicted):
        raise ValueError("sample and predicted values must have same length")
    rms_ = sum((y_sample[i]-y_predicted[i])**2 for i in range(len(y_sample)))
    return sqrt(rms_/len(y_sample))
