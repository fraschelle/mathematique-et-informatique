#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Numerical evaluations of the differential of a numerical function.
"""

from typing import List, Tuple


def forward(x: List[float], y: List[float]) -> Tuple[List[float], List[float]]:
    """
    Evaluate the derivative using the forward differentiation strategy.
    
    Parameters
    ----------
    x : List[float]
        the absissa coordinates of the numerical function.
    y : List[float]
        the ordinate coordinate of the numerical function.

    Raises
    ------
    ValueError
        In case the lengths of x and y are not equal.

    Returns
    -------
    (Tuple[List[float], List[float]])
        First element is the truncate sequence of absissa, since one calculate difference of them, one can not attribute a differential to all absissa.
        Second element is the calculated differential associated to the related absissa given in the first element of the tuple.
    """
    if not len(x) == len(y):
        raise ValueError("x and y ust have same length")
    if len(x) < 2:
        return [], []
    forward = [(y[i+1] - y[i]) / (x[i+1] - x[i]) for i in range(len(y)-1)]
    return x[:-1], forward

def backward(x, y):
    """
    Evaluate the derivative using the backward differentiation strategy.
    
    Parameters
    ----------
    x : List[float]
        the absissa coordinates of the numerical function.
    y : List[float]
        the ordinate coordinate of the numerical function.

    Raises
    ------
    ValueError
        In case the lengths of x and y are not equal.

    Returns
    -------
    (Tuple[List[float], List[float]])
        First element is the truncate sequence of absissa, since one calculate difference of them, one can not attribute a differential to all absissa.
        Second element is the calculated differential associated to the related absissa given in the first element of the tuple.
        """
    if not len(x) == len(y):
        raise ValueError("x and y ust have same length")
    if len(x) < 2:
        return [], []
    backward = [(y[i] - y[i-1]) / (x[i] - x[i-1]) for i in range(1, len(y))]
    return x[1:], backward

def central(x, y):
    """
    Evaluate the derivative using the central difference differentiation strategy.
    
    Parameters
    ----------
    x : List[float]
        the absissa coordinates of the numerical function.
    y : List[float]
        the ordinate coordinate of the numerical function.

    Raises
    ------
    ValueError
        In case the lengths of x and y are not equal.

    Returns
    -------
    (Tuple[List[float], List[float]])
        First element is the truncate sequence of absissa, since one calculate difference of them, one can not attribute a differential to all absissa.
        Second element is the calculated differential associated to the related absissa given in the first element of the tuple.
        """
    if not len(x) == len(y):
        raise ValueError("x and y ust have same length")
    if len(x) < 2:
        return [], []
    central = [(y[i+1] - y[i-1]) / (x[i+1] - x[i-1])
               for i in range(1, len(y)-1)]
    return x[1:-1], central

def weighted(x, y):
    """
    Evaluate the derivative using a weighted point strategy.
    
    Parameters
    ----------
    x : List[float]
        the absissa coordinates of the numerical function.
    y : List[float]
        the ordinate coordinate of the numerical function.

    Raises
    ------
    ValueError
        In case the lengths of x and y are not equal.

    Returns
    -------
    (Tuple[List[float], List[float]])
        First element is the truncate sequence of absissa, since one calculate difference of them, one can not attribute a differential to all absissa.
        Second element is the calculated differential associated to the related absissa given in the first element of the tuple.
    """
    if not len(x) == len(y):
        raise ValueError("x and y ust have same length")
    if len(x) < 2:
        return [], []
    derivative = [((x[i] - x[i-1]) * (y[i+1] - y[i]) / (x[i+1] - x[i])
                   + (x[i+1] - x[i]) * (y[i] - y[i-1]) / (x[i] - x[i-1])) \
                  / (x[i+1] - x[i-1])
                  for i in range(1, len(y)-1)]
    return x[1:-1], derivative
